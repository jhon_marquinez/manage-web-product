﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GestArtWeb.Startup))]
namespace GestArtWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
