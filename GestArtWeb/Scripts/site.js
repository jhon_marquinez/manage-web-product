﻿/**
 * Set the max height to edit container
 * **/
function settingMaxHeightEditSection() {
    var containerAttriubutes = $('.body-content').find('#attribute-input-container');
    if (containerAttriubutes.attr('id') != undefined) {
        containerAttriubutes.attr("style", "max-height: " + ($(window).height() - containerAttriubutes.data("value-less-height")) + "px")
    }
}


(function () {
    const slinky = $('#menu').slinky();

    $("#formSeach").on("submit", function (e) {
        e.preventDefault();
        $.ajax({
            beforeSend: function () {
                $("#loadingSearchProduct").show();
            },
            data: { ajaxQuery: true, skuOrName: $("#inputSearch").val() },
            url: "/",
            type: "get",
            success: function (result, status, xhr) {
                $("#resultAjax").html(result);
                productEventSet(SECTION_PRODUCT_VIEW);
                settingMaxHeightEditSection();
            },
            error: function (xhr, status, error) {
                alertify.errorAlert("Código de error: <b>" + xhr.status + "</b><br> Mensaje de error: <b>" + xhr.statusText + "</b>.<br>Vuelva a <b>intentarlo</b>, contacte con el departamento de IT si el error persiste. <br> Gracias.");
            },
            complete: function () {
                $("#loadingSearchProduct").hide();
            }
        });

    });

    // Product
    productEventSet('/');

    $('#menu-item-view-products').on('click', function (e) {
        e.preventDefault();
        var sectionQuery = $(this).attr("href");
        $.ajax({
            data: { ajaxQuery: true },
            url: sectionQuery,
            type: "get",
            success: function (result, status, xhr) {
                $("#resultAjax").html(result);
                $('.body-content').trigger('click');
                productEventSet(sectionQuery);
                settingMaxHeightEditSection();
            },
            error: function (xhr, status, error) {
                alertify.errorAlert("Código de error: <b>" + xhr.status + "</b><br> Mensaje de error: <b>" + xhr.statusText + "</b>.<br>Vuelva a <b>intentarlo</b>, contacte con el departamento de IT si el error persiste. <br> Gracias.");
            }
        });
    });

    $('#menu-item-create-product').on('click', function (e) {
        e.preventDefault();
        var sectionQuery = $(this).attr("href");
        $.ajax({
            url: sectionQuery,
            type: "get",
            success: function (result, status, xhr) {
                $("#resultAjax").html(result);
                $('.body-content').trigger('click');
                productEventSet(sectionQuery);
                settingMaxHeightEditSection();
            },
            error: function (xhr, status, error) {
                alertify.errorAlert("Código de error: <b>" + xhr.status + "</b><br> Mensaje de error: <b>" + xhr.statusText + "</b>.<br>Vuelva a <b>intentarlo</b>, contacte con el departamento de IT si el error persiste. <br> Gracias.");
            }
        });
    });

    $('#menu-item-update-photo-product').on('click', function (e) {
        e.preventDefault();
        var sectionQuery = $(this).attr("href");
        $.ajax({
            url: sectionQuery,
            type: "get",
            success: function (result, status, xhr) {
                $("#formUpdatePhoto").remove();
                $("body").append(result);
                alertify.genericDialog($('#formUpdatePhoto').show()[0]).set('selector', 'textarea[name="listCodeProduct"]');
                productEventSet(sectionQuery);
            },
            error: function (xhr, status, error) {
                alertify.errorAlert("Código de error: <b>" + xhr.status + "</b><br> Mensaje de error: <b>" + xhr.statusText + "</b>.<br>Vuelva a <b>intentarlo</b>, contacte con el departamento de IT si el error persiste. <br> Gracias.");
            }
        });
    });


    // category
    $('#menu-item-view-categories').on('click', function (e) {
        e.preventDefault();
        var sectionQuery = $(this).attr("href");
        $.ajax({
            url: sectionQuery,
            type: "get",
            success: function (result, status, xhr) {
                $("#resultAjax").html(result);
                $('.body-content').trigger('click');
                setHandlerItemPagerCategory();
            },
            error: function (xhr, status, error) {
                alertify.errorAlert("Código de error: <b>" + xhr.status + "</b><br> Mensaje de error: <b>" + xhr.statusText + "</b>.<br>Vuelva a <b>intentarlo</b>, contacte con el departamento de IT si el error persiste. <br> Gracias.");
            }
        });
    });

    $("#menu-item-create-category").on("click", function (e) {
        e.preventDefault();
        var sectionQuery = $(this).attr("href");
        $.ajax({
            url: sectionQuery,
            type: "get",
            success: function (result, status, xhr) {
                $("#formNewCategory").remove();
                $("body").append(result);
                alertify.genericDialog($('#formNewCategory').show()[0]).set('selector', 'input[name="name"]');
            },
            error: function (xhr, status, error) {
                alertify.errorAlert("Código de error: <b>" + xhr.status + "</b><br> Mensaje de error: <b>" + xhr.statusText + "</b>.<br>Vuelva a <b>intentarlo</b>, contacte con el departamento de IT si el error persiste. <br> Gracias.");
            },
            complete: function () {
                setHandlerBtnCreateNewCategory();
            }
        });

    });

    $("#menu-item-edit-category").on("click", function (e) {
        e.preventDefault();
        var sectionQuery = $(this).attr("href");
        $.ajax({
            url: sectionQuery,
            type: "get",
            success: function (result, status, xhr) {
                $("#formEditCategory").remove();
                $("body").append(result);
                alertify.genericDialog($('#formEditCategory').show()[0]).set('selector', 'input[name="entityId"]');
            },
            error: function (xhr, status, error) {
                alertify.errorAlert("Código de error: <b>" + xhr.status + "</b><br> Mensaje de error: <b>" + xhr.statusText + "</b>.<br>Vuelva a <b>intentarlo</b>, contacte con el departamento de IT si el error persiste. <br> Gracias.");
            },
            complete: function () {
                setHandlerFomrEditCategory();
            }
        });

    });

 
    function setHandlerFomrEditCategory() {
        setHandlerEditFormInputEntityId();

        $("#nameCat").on("focusin", function () {
            $(this).removeClass("is-invalid");
            $(this).removeClass("is-valid");
        });

        $("#idPadre").on("focusin", function () {
            $(this).removeClass("is-invalid");
            $(this).removeClass("is-valid");
        });

        $("#btnEditCategoty").on("click", function (e) {
            e.preventDefault();

            if ($("#nameCat").val().length == 0) {
                $("#nameCat").addClass("is-invalid");
                $("#messageSuccessResultFormEditCategory").parents(".card").hide();
                $("#messageErrorResultFormEditCategory").html("!El nombre de la nueva categoría son requeridos¡");
                $("#messageErrorResultFormEditCategory").parents(".card").show();
                return;
            } else if ($("#idPadre").val().length == 0) {
                $("#idPadre").addClass("is-invalid");
                $("#messageSuccessResultFormEditCategory").parents(".card").hide();
                $("#messageErrorResultFormEditCategory").html("!El padre de la nueva categoría son requeridos¡");
                $("#messageErrorResultFormEditCategory").parents(".card").show();
                return;
            }

            if ($("#is_active").is(":checked")) {
                $("#is_active").val(1);
            }
            
            var dataCategory = decodeURIComponent($("#formEditCategory").serialize());
            $.ajax({
                beforeSend: function () {
                    $("#loadingUpdateCategory").show();
                },
                data: {
                    dataUpdateCategory: dataCategory
                },
                url: "/Categoria/Edit",
                type: "post",
                success: function (messages, status, xhr) {

                    if (messages.Type == "Success") {
                        alertify.success("¡Se ha actualizado correctamente la nueva categoría!");
                        $("#messageErrorResultFormEditCategory").parents(".card").hide();
                        $("#messageSuccessResultFormEditCategory").html(messages.Messages);
                        $("#messageSuccessResultFormEditCategory").parents(".card").show();
                    }
                    else {
                        alertify.error("!Actualización fallida¡");
                        $("#messageSuccessResultCreateCategory").parents(".card").hide();
                        $("#messageErrorResultFormEditCategory").html(messages.Messages);
                        $("#messageErrorResultFormEditCategory").parents(".card").show();
                    }
                },
                error: function (xhr, status, error) {
                    alertify.alert("INFORMACIÓN DE ERROR", "Código de error: " + xhr.status +
                        ".<br> Mensaje de error: " + xhr.statusText +
                        ".<br>Contacte con el departamento de IT si el error persiste. <br> Gracias.");
                },
                complete: function () {
                    $("#loadingUpdateCategory").hide();
                }
            });
            
        });
    }

    function setHandlerEditFormInputEntityId() {
        $("#entityId").on("focusin", function () {
            $(this).removeClass("is-invalid");
            $(this).removeClass("is-valid");
            $("#messageSuccessResultFormEditCategory").parents(".card").hide();
            $("#messageErrorResultFormEditCategory").parents(".card").hide();
            $("#nameCat").val("");
            $("#idPadre").val("");
            $("#is_active").attr("checked", false);
            $("#btnEditCategoty").attr("disabled", "disabled");
        });

        $("#entityId").on("focusout", function () {
            var inputEntityId = $(this);

            if (inputEntityId.val().length == 0) {
                inputEntityId.addClass("is-invalid");
                $("#messageSuccessResultFormEditCategory").parents(".card").hide();
                $("#messageErrorResultFormEditCategory").html("El código identificativo es requerido, introduzca el código de la categoría");
                $("#messageErrorResultFormEditCategory").parents(".card").show();
                return;
            }

            $.ajax({
                beforeSend: function () { $("#loadingCheckCategory").show(); },
                data: { id: inputEntityId.val() },
                url: "/Categoria/Edit",
                type: "get",
                success: function (catFound, status, xhr) {
                    if (catFound.Exists == "1") {
                        inputEntityId.removeClass("is-invalid");
                        inputEntityId.addClass("is-valid");
                        $("#nameCat").val(catFound.Name);
                        $("#idPadre").val(catFound.Parent);
                        if (catFound.Active == "1") {
                            $("#is_active").attr("checked", true);
                            $("#is_active").val(1);
                        } else {
                            $("#is_active").attr("checked", false);
                            $("#is_active").val(0);
                        }
                        $("#btnEditCategoty").removeAttr("disabled");
                        $("#messageErrorResultFormEditCategory").parents(".card").hide();
                    } else {
                        inputEntityId.addClass("is-invalid");
                        $("#messageSuccessResultFormEditCategory").parents(".card").hide();
                        $("#messageErrorResultFormEditCategory").html(catFound.Messages);
                        $("#messageErrorResultFormEditCategory").parents(".card").show();
                        $("#nameCat").val("");
                        $("#idPadre").val("");
                        $("#is_active").removeAttr('checked');
                        $("#btnEditCategoty").attr("disabled","disabled");
                    }
                },
                error: function (xhr, status, error) {
                    alertify.errorAlert("Código de error: <b>" + xhr.status + "</b><br> Mensaje de error: <b>" + xhr.statusText + "</b>.<br>Vuelva a <b>intentarlo</b>, contacte con el departamento de IT si el error persiste. <br> Gracias.");
                },
                complete: function () { $("#loadingCheckCategory").hide(); }
            });

        });
    }

    function setHandlerItemPagerCategory() {
        $(".item-pager").on("click", function (e) {
            e.preventDefault();
            var sectionQuery = $(this).attr("href");
            $.ajax({
                url: sectionQuery,
                type: "get",
                success: function (result, status, xhr) {
                    $("#resultAjax").html(result);
                    $('.body-content').trigger('click');
                    setHandlerItemPagerCategory();
                },
                error: function (xhr, status, error) {
                    alertify.errorAlert("Código de error: <b>" + xhr.status + "</b><br> Mensaje de error: <b>" + xhr.statusText + "</b>.<br>Vuelva a <b>intentarlo</b>, contacte con el departamento de IT si el error persiste. <br> Gracias.");
                }
            });
        });
    }

    function setHandlerBtnCreateNewCategory() {

        $("#nameCat").on("focusin", function () {
            $("#nameCat").removeClass("is-invalid");
        });

        $("#idPadre").on("focusin", function () {
            $("#idPadre").removeClass("is-invalid");
        });

        $("#btnCreateNewCategoty").on("click", function (e) {
            e.preventDefault();

            if ($("#nameCat").val().length == 0) {
                $("#nameCat").addClass("is-invalid");
                $("#messageSuccessResultCreateCategory").parents(".card").hide();
                $("#messageErrorResultCreateCategory").html("!El nombre de la nueva categoría son requeridos¡");
                $("#messageErrorResultCreateCategory").parents(".card").show();
                return;
            } else if ($("#idPadre").val().length == 0) {
                $("#idPadre").addClass("is-invalid");
                $("#messageSuccessResultCreateCategory").parents(".card").hide();
                $("#messageErrorResultCreateCategory").html("!El padre de la nueva categoría son requeridos¡");
                $("#messageErrorResultCreateCategory").parents(".card").show();
                return;
            }
            

            var dataCategory = decodeURIComponent($("#formNewCategory").serialize());

            $.ajax({
                beforeSend: function () {
                    $("#loadingCreateCategory").show();
                },
                data: {
                    dataNewCategory: dataCategory
                },
                url: "/Categoria/Create",
                type: "post",
                success: function (messages, status, xhr) {
                                        
                    if (messages.Type == "Success") {
                        alertify.success("¡Se ha creado correctamente la nueva categoría!");
                        $("#messageErrorResultCreateCategory").parents(".card").hide();
                        $("#messageSuccessResultCreateCategory").html(messages.Messages);
                        $("#messageSuccessResultCreateCategory").parents(".card").show();
                    }
                    else {
                        alertify.error("!Creación fallida¡");
                        $("#messageSuccessResultCreateCategory").parents(".card").hide();
                        $("#messageErrorResultCreateCategory").html(messages.Messages);
                        $("#messageErrorResultCreateCategory").parents(".card").show();
                    }
                },
                error: function (xhr, status, error) {
                    alertify.alert("INFORMACIÓN DE ERROR", "Código de error: " + xhr.status +
                        ".<br> Mensaje de error: " + xhr.statusText +
                        ".<br>Contacte con el departamento de IT si el error persiste. <br> Gracias.");
                },
                complete: function () {
                    $("#loadingCreateCategory").hide();
                }
            });
        });
        
    }

    // fin category

    $('.ico-menu').on('click', function () {
        $('.body-content').toggleClass('menu-open');
        setTimeout(function () {
            $('.body-content').toggleClass('can-close-menu');
        }, 250);
    });

    $('.body-content').on('click', function () {
        var bodyConeten = $('.body-content');
        if (bodyConeten.hasClass('can-close-menu')) {
            bodyConeten.removeClass('menu-open');
            bodyConeten.removeClass('can-close-menu');
        }
    });

    

    if (!alertify.errorAlert) {
        //define a new errorAlert base on alert
        alertify.dialog('errorAlert', function factory() {
            return {
                build: function () {
                    var errorHeader = '<span class="fa fa-times-circle fa-2x" '
                        + 'style="vertical-align:middle;color:#e10000;">'
                        + '</span> ERROR ENCONTRADO';
                    this.setHeader(errorHeader);
                }
            };
        }, true, 'alert');
    }

    if (!alertify.correctAlert) {
        //define a new errorAlert base on alert
        alertify.dialog('correctAlert', function factory() {
            return {
                build: function () {
                    var errorHeader = '<span class="fa fa-check-circle fa-2x" '
                        + 'style="vertical-align:middle;color:#009933;">'
                        + '</span> INFORMACIÓN';
                    this.setHeader(errorHeader);
                }
            };
        }, true, 'alert');
    }

    if (!alertify.warningAlert) {
        //define a new errorAlert base on alert
        alertify.dialog('warningAlert', function factory() {
            return {
                build: function () {
                    var errorHeader = '<span class="fa fa-exclamation-triangle fa-2x" '
                        + 'style="vertical-align:middle;color:#ff9933;">'
                        + '</span> ADVERTENCIA';
                    this.setHeader(errorHeader);
                }
            };
        }, true, 'alert');
    }


    if (!alertify.questionAlert) {
        //define a new errorAlert base on alert
        alertify.dialog('questionAlert', function factory() {
            return {
                build: function () {
                    var errorHeader = '<span class="fa fa-question-circle fa-2x" '
                        + 'style="vertical-align:middle;color:#446E9B;">'
                        + '</span> INFORMACIÓN';
                    this.setHeader(errorHeader);
                }
            };
        }, true, 'alert');
    }


    if (!alertify.infoAlert) {
        //define a new errorAlert base on alert
        alertify.dialog('infoAlert', function factory() {
            return {
                build: function () {
                    var errorHeader = '<span class="fa fa-info-circle fa-2x" '
                        + 'style="vertical-align:middle;color:#446E9B;">'
                        + '</span> INFORMACIÓN';
                    this.setHeader(errorHeader);
                }
            };
        }, true, 'alert');
    }


    alertify.genericDialog || alertify.dialog('genericDialog', function () {
        return {
            main: function (content) {
                this.setContent(content);
            },
            setup: function () {
                return {
                    focus: {
                        element: function () {
                            return this.elements.body.querySelector(this.get('selector'));
                        },
                        select: true
                    },
                    options: {
                        basic: true,
                        maximizable: false,
                        resizable: false,
                        padding: false
                    }
                };
            },
            settings: {
                selector: undefined
            }
        };
    });


    settingMaxHeightEditSection();
})()

