﻿$(document).ready()
{


    addEventTopMenuRowInList();
   

    


    // FUNCTIONS
    /**
     * Set the event click to all menu in categories listing
     * **/ 
    function addEventTopMenuRowInList() {
        $(".menuAction").click(function () {
            $(".dropdown-menu").hide("fast", "swing");
            if ($(this).hasClass("show"))
                $(this).removeClass("show").addClass("hide").children(".dropdown-menu").hide("fast", "swing");
            else
                $(this).removeClass("show").addClass("show").children(".dropdown-menu").show("fast", "swing");
        })
    }

  
    //  EVENTS
    /**
     * Event to search categories press the any key and show in the categories listing
     * **/
    $("#searchValue").on("keyup", function () {

        var searchValue = $(this).val();
        if (searchValue != "")
            $.ajax({
                data: { "searchValue": searchValue },
                url: "/Categoria/ListFilter",
                type: "get",
                success: function (result, status, xhr) {
                   $("#listFiltered").empty().append(result);
                },
                error: function (xhr, status, error) {
                    alertify.alert("INFORMACIÓN DE ERROR", "Código de error: " + xhr.status +
                        ".<br> Mensaje de error: " + xhr.statusText +
                        ".<br>Contacte con el departamento de IT si el error persiste. <br> Gracias.");                    
                },
                complete: addEventTopMenuRowInList 
            });
        else
            $.get("/Categoria/ListFilter", function (result, status, xhr) {
                $("#listFiltered").html(result);
            }).fail(function (xhr, status, error) {
                alertify.alert("INFORMACIÓN DE ERROR", "Código de error: " + xhr.status +
                    ".<br> Mensaje de error: " + xhr.statusText +
                        ".<br>Contacte con el departamento de IT si el error persiste. <br> Gracias.");
            }).done(addEventTopMenuRowInList);
    })
    

    /**
     * Load subcategories for the detail view category
     * **/
    $("#btnProductOfCategories").on("click", function (e) {
        e.preventDefault();
        $(this).addClass("active");
        $("#btnSubCategories").removeClass("active");

        $.ajax({
            beforeSend: function (xhr) {
                $("#cardBodyProductsAndCategories").empty().append('<img src="/fonts/loading3.gif" class="img-responsive mx-auto m-5" />').hide().show("fade","slow");
            },
            url: "/Categoria/ProductsOfCategory/" + $("#IdCategory").val(),
            type: "get",
            success: function (result, status, xhr) {
                $("#cardBodyProductsAndCategories").empty().append(result).hide().show("puff", "fast");
            },
            error: function (xhr, status, error) {
                alertify.alert("INFORMACIÓN DE ERROR", "Código de error: " + xhr.status +
                    ".<br> Mensaje de error: " + xhr.statusText +
                    ".<br>Contacte con el departamento de IT si el error persiste. <br> Gracias.");
            }
        });
    });

    /**
     * Load the product of one category for the view detail category
     * **/
    $("#btnSubCategories").on("click", function (e) {
        e.preventDefault();
        $(this).addClass("active");
        $("#btnProductOfCategories").removeClass("active");

        $.ajax({
            url: "/Categoria/SubCategorias/" + $("#IdCategory").val(),
            type: "get",
            success: function (result, status, xhr) {
                $("#cardBodyProductsAndCategories").empty().append(result).hide().show("puff", "fast");
            },
            error: function (xhr, status, error) {
                alertify.alert("INFORMACIÓN DE ERROR", "Código de error: " + xhr.status +
                    ".<br> Mensaje de error: " + xhr.statusText +
                    ".<br>Contacte con el departamento de IT si el error persiste. <br> Gracias.");
            }
        });
    });

























    /*

    function getTree() {
        // Some logic to retrieve, or generate tree structure
        return data;
    }
    */

    var tree = [
        
            {
            text: "Node 1",
            icon: "glyphicon glyphicon-stop",
            selectedIcon: "glyphicon glyphicon-stop",
            color: "#000000",
            backColor: "#FFFFFF",
            href: "#node-1",
            selectable: false,
            state: {
                checked: false,
                disabled: false,
                expanded: false,
                selected: false
            },
            nodes: [
                {
                    text: "Child 1",
                    nodes: [
                        {
                            text: "Grandchild 1"
                        },
                        {
                            text: "Grandchild 2"
                        }
                    ]
                },
                {
                    text: "Child 2"
                }
            ]
        },
        {
            text: "Parent 2"
        },
        {
            text: "Parent 3"
        },
        {
            text: "Parent 4"
        },
        {
            text: "Parent 5"
        }
    ];


    var defaultData = [
        {
            text: 'Parent 1',
            href: '#parent1',
            tags: ['4'],
            nodes: [
                {
                    text: 'Child 1',
                    href: '#child1',
                    tags: ['2'],
                    nodes: [
                        {
                            text: 'Grandchild 1',
                            href: '#grandchild1',
                            tags: ['0']
                        },
                        {
                            text: 'Grandchild 2',
                            href: '#grandchild2',
                            tags: ['0']
                        }
                    ]
                },
                {
                    text: 'Child 2',
                    href: '#child2',
                    tags: ['0']
                }
            ]
        },
        {
            text: 'Parent 2',
            href: '#parent2',
            tags: ['0']
        },
        {
            text: 'Parent 3',
            href: '#parent3',
            tags: ['0']
        },
        {
            text: 'Parent 4',
            href: '#parent4',
            tags: ['0']
        },
        {
            text: 'Parent 5',
            href: '#parent5',
            tags: ['0']
        }
    ];

    var alternateData = [
        {
            text: 'Parent 1',
            tags: ['2'],
            nodes: [
                {
                    text: 'Child 1',
                    tags: ['3'],
                    nodes: [
                        {
                            text: 'Grandchild 1',
                            tags: ['6']
                        },
                        {
                            text: 'Grandchild 2',
                            tags: ['3']
                        }
                    ]
                },
                {
                    text: 'Child 2',
                    tags: ['3']
                }
            ]
        },
        {
            text: 'Parent 2',
            tags: ['7']
        },
        {
            text: 'Parent 3',
            icon: 'glyphicon glyphicon-earphone',
            href: '#demo',
            tags: ['11']
        },
        {
            text: 'Parent 4',
            icon: 'glyphicon glyphicon-cloud-download',
            href: '/demo.html',
            tags: ['19'],
            selected: true
        },
        {
            text: 'Parent 5',
            icon: 'glyphicon glyphicon-certificate',
            color: 'pink',
            backColor: 'red',
            href: 'http://www.tesco.com',
            tags: ['available', '0']
        }
    ];
    


    var $mytree = $('#tree').treeview({

        data: defaultData,
        showIcon: false,
        showCheckbox: true,
       
        // enables links
        enableLinks: false,

        // enables multi select
        multiSelect: false,

        onNodeChecked: function (event, node) {
            tmp(node);
        }


    });

    var tmp = function (node) {
        $mytree.treeview('uncheckAll', { silent: $('#chk-check-silent').is(':checked') });
        $mytree.treeview('checkNode', [node.nodeId, { silent: true }]);
    }


    var findCheckableNodessJ = function () {
        return $mytree.treeview('search', [$('#input-check-node').val(), { ignoreCase: false, exactMatch: false }]);
    };
    var checkableNodes = findCheckableNodessJ();

    $('#input-check-node').on('keyup', function (e) {
        checkableNodes = findCheckableNodessJ();
        $('.check-node').prop('disabled', !(checkableNodes.length >= 1));
    });










}