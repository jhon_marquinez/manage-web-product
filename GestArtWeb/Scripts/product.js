﻿const ERROR_FILE_EXCEEDS_LIMIT_STORAGE = "El fichero Excel no debe tener un tamaño de almacenamiento superior a 1 GB.";
const ERROR_NOT_MESSAGE_RECEIVE = "No se ha recivido ningún mensaje del proceso ejecutado actualemente. Intentelo de nuevo y si persite el error contacte con el departamento de IT.";
const ERROR_FILE_NOT_SENT = "Es requerido que antes de enviar el fichero Excel primero debe de seleccionarlo. <br><br>SOLUCIÓN: <br>1) Ha de dar clic en el botón de <b>Seleccionar archivo</b> y elegir un fichero excel almacenado en algún dispositivo de almacenamiento conectado al sistema <i>(recuerde que los ficheros excel son de extención <b>.xls</b> o <b>.xlsx</b>)<i>.";
const TEMPS_TO_INITIALIZE_FUNCTION_SELECTNEWCATS = 50;
const TEMPS_TO_INITIALIZE_FUNCTION_SETVALUEFORNEWPRODUCTBASEDINCOPYATTRIBUTE = 60;
const SECTION_PRODUCT_CREATE = "/Producto/Create";
const SECTION_PRODUCT_VIEW = "/";
const SECTION_PRODUCT_EDIT = "/Producto/Edit";
const SECTION_PRODUCT_UPDATE_PHOTO = "/Producto/FormUpdatePhoto";

function showMessageReceived(messageForCreateThroghExcel) {
    if ($("#messageError").length > 0) {
        if ($("#messageError").find('table').length > 0) {

            alertify.errorAlert($("#messageError")[0].innerHTML);

            if ($(".messageAction").length > 0) {
                $(".messageAction").on("click", function () {
                    alertify.error("You must programme this action to this element! data-id: " + $(this).data("id") + " and data-action: " + $(this).data("action"));
                });
            }
        }
        else {
            if (messageForCreateThroghExcel != undefined && messageForCreateThroghExcel == true) {

                alertify.errorAlert($("#messageError")[0].innerHTML);
            } else {
                alertify.errorAlert($("#messageError").text());
            }
        }
    }

    if ($("#messageWarning").length > 0) {

        if ($("#messageWarning").find('table').length > 0) {

            alertify.warningAlert($("#messageWarning")[0].innerHTML);

            if ($(".messageAction").length > 0) {
                $(".messageAction").on("click", function () {
                    alertify.error("You must programme this action to this element! data-id: " + $(this).data("id") + " and data-action: " + $(this).data("action"));
                });
            }
        }
        else {
            if (messageForCreateThroghExcel != undefined && messageForCreateThroghExcel == true) {

                alertify.errorAlert($("#messageWarning")[0].innerHTML);
            } else {
                alertify.errorAlert($("#messageWarning").text());
            }
        }
    }


    if ($("#messageSuccess").length > 0) {

        if ($("#messageSuccess").find('table').length > 0) {

            alertify.correctAlert($("#messageSuccess")[0].innerHTML);

            if ($(".messageAction").length > 0) {
                $(".messageAction").on("click", function () {
                    alertify.error("You must programme this action to this element! data-id: " + $(this).data("id") + " and data-action: " + $(this).data("action"));
                });
            }
        }
        else {
            if (messageForCreateThroghExcel != undefined && messageForCreateThroghExcel == true) {

                alertify.errorAlert($("#messageSuccess")[0].innerHTML);
            } else {
                alertify.errorAlert($("#messageSuccess").text());
            }
        }
    }

}

function setEventChangeForListCat2() {
    $("#cat2").on("change", function () {
        setSelectListCat("cat2", $(this).find(':selected').val());
    });
}

function copyFamily(product)
{
    var selectFamilies = $("#id_familia");
    var families = selectFamilies.children("option");
    var numFamilies = families.length;
    var a = 0;
    families.attr('selected', false);
    do {
        if (families.eq(a).val() == product.id_familia) {
            families.eq(a).attr('selected', true);
            selectFamilies.trigger("change");
            break;
        }
        a++;
    } while (a < numFamilies);
}


function selectOptionThroghValue(select, value, execEventChange)
{
    var options = select.children("option");
    var a = 0;
    options.attr('selected', false);
    do {
        if (options.eq(a).val() == value) {
            options.eq(a).attr('selected', true);
            if (execEventChange){
                select.trigger("change");
            }
            break;
        }
        a++;
    } while (a < options.length);
}

function SelectNewCats(initialCat, mageCats) {

    if (initialCat > 3) return;

    var cat = $("#cat" + initialCat);
    var catOptions = cat.children("option");
    catOptions.attr("selected", false);
    var mageCat;

    for (var e = 0; e < mageCats.length; e++) {
        if (mageCats[e].nivel == initialCat+1) {
            mageCat = mageCats[e];
            break;
        }
    }

    selectOptionThroghValue(cat, mageCat.entity_id, true);
    setTimeout(function () { SelectNewCats(initialCat + 1, mageCats) }, TEMPS_TO_INITIALIZE_FUNCTION_SELECTNEWCATS);

}

function SetValueForNewProductBasedInCopyAttribute(attributesProductCopy) {
    setTimeout(function () {
        for (var a = 0; a < attributesProductCopy.length; a++) {
            var attrVal = attributesProductCopy[a];
            var input = $("#attribute-input-container").find("#" + attrVal.id_atributo);

            if (input.is("input") || input.is("textarea")) {
                if (attrVal.value_varchar != null) { input.val(attrVal.value_varchar); }
                else if (attrVal.value_text != null) { input.val(attrVal.value_text); }
                else if (attrVal.value_int != null) { input.val(attrVal.value_int); }
                else { input.val(attrVal.value_decimal); }

            } else {
                selectOptionThroghValue(input, attrVal.value_int, false);
            }
        }
    }, TEMPS_TO_INITIALIZE_FUNCTION_SETVALUEFORNEWPRODUCTBASEDINCOPYATTRIBUTE);
}

function setEventForTrCopyData() {
    $(".trProductToCopyData").on("click", function () {

        $.ajax({
            data: { skuOrName: $(this).data("id-product") },
            url: "/api/productos",
            type: "get",
            success: function (products, status, xhr) {
                var i = 0;
                do {
                    var product = products[i];
                    
                    selectOptionThroghValue($("#id_familia"), product.id_familia, true);

                    SelectNewCats(1, product.mage_categoria);

                    SetValueForNewProductBasedInCopyAttribute(product.mage_articulo_atributo_valor);

                    i++;
                } while (i < products.length);
            },
            error: function (xhr, status, error) {
                alertify.alert("INFORMACIÓN DE ERROR", "Código de error: " + xhr.status +
                    ".<br> Mensaje de error: " + xhr.statusText +
                    ".<br>Contacte con el departamento de IT si el error persiste. <br> Gracias.");
            }
        });

    });
}

function setSelectListCat(idCat, catSelected) {
    $.ajax({
        data: {
            parent: catSelected
        },
        url: "/Producto/GetSelectListCategoriesForOnlyParent",
        type: "get",
        success: function (result, status, xhr) {
            if (idCat == "cat1") {
                $("#containerListCat2").html(result);
                setEventChangeForListCat2();
                $("#cat2").trigger("change");
            }else {
                $("#containerListCat3").html(result);
            }
            
        },
        error: function (xhr, status, error) {
            alertify.alert("INFORMACIÓN DE ERROR", "Código de error: " + xhr.status +
                ".<br> Mensaje de error: " + xhr.statusText +
                ".<br>Contacte con el departamento de IT si el error persiste. <br> Gracias.");
        }
    });
}

function commonFunctionInSectionsCreateAndEdit() {
    setEventForTrCopyData();

    $("#inputSearchProductToCopyData").on("keyup", function () {
        $.ajax({
            data: { skuOrName: $(this).val() },
            url: "/Producto/GetProductByIdOrName",
            type: "get",
            success: function (result, status, xhr) {
                $("#containerTableCopyDataProduct").html(result);
                setEventForTrCopyData();
            },
            error: function (xhr, status, error) {
                alertify.alert("INFORMACIÓN DE ERROR", "Código de error: " + xhr.status +
                    ".<br> Mensaje de error: " + xhr.statusText +
                    ".<br>Contacte con el departamento de IT si el error persiste. <br> Gracias.");
            }
        });
    });

    setEventChangeForListCat2();
    $("#cat1").on("change", function () {
        if ($("#containerListCat1").data("set-event-change-first-time") == "False") {
            $("#containerListCat1").data("set-event-change-first-time", true)

        } else {
            setSelectListCat("cat1", $(this).find(':selected').val());
        }
    });
    $("#cat1").trigger("change");
}


function getTableMessage(listMessage, typeMessageTable) {
    var messageTable = "";
    if (listMessage != null) {

        if (listMessage.length == 1) {
            messageTable += '<div id="' + typeMessageTable + '" style="display:none">' + listMessage[0].message + '</div>';
        }
        else {
            messageTable += '<div id ="' + typeMessageTable + '" style = "display:none"><table class="table"><thead class="thead-dark"><tr><th scope="col">Código</th><th scope="col">Información</th><th scope="col"></th></tr></thead><tbody>';
            for (var i = 0; i < listMessage.length; i++) {
                var buttonAction = listMessage[i].btnAction == null ? "" : listMessage[i].btnAction;
                var code = listMessage[i].code == null ? "" : listMessage[i].code;
                var message = listMessage[i].message == null ? "" : listMessage[i].message;
                messageTable += '<tr><td>' + code + '</td><td>' + message + '</td><td>' + buttonAction + '</td></tr>';
            }
            messageTable += '</tbody></table></div>';
        }

    }
    return messageTable;
}

function setHandlerToBtnUpdateDocument() {
    $("#updateDocument").on("click", function (e) {
        e.preventDefault();

        console.info($("#nameDoc").val());
        console.info($("#sku").val());
        
        $.ajax({
            beforeSend: function () { $("#loadingMessageForUpdateDocument").show(); },
            data: { sku: $("#sku").val(), nameDoc: $("#nameDoc").val() },
            url: "/Producto/RefreshcumentProduct",
            type: "post",
            success: function (result, status, xhr) {
                console.info(result);
                if (result == 1) {
                    alertify.correctAlert("Si el documento PDF<i>(ficha técnica)</i> del producto esta subido en la estructura de archivos de la web mediante el cliente FTP en la siguiente ruta: <b>/media/pdf_products_import</b> <br>La ficha técnica ya esta acutalizada, verifiquelo en la web.");
                } else {
                    alertify.error("No se ha actualizado, intentelo de nuevo");
                }
            },
            error: function (xhr, status, error) {
                alertify.errorAlert("Código de error: <b>" + xhr.status + "</b><br> Mensaje de error: <b>" + xhr.statusText + "</b>.<br>Vuelva a <b>intentarlo</b>, contacte con el departamento de IT si el error persiste. <br> Gracias.");
            },
            complete: function () {
                $("#loadingMessageForUpdateDocument").hide();
            }
        });
    });
}


function getImageSelect() {
    var files = $("#inputFileImages").get(0).files;
    if (files.length > 0) {
        var fileImages = new FormData();
        for (var a = 0; a < files.length; a++) {
            fileImages.append(files[a].name, files[a]);
        }
        // get skus the textarea and add to formData
        // buscar la forma de enviar los skus tambíen
    }
    return fileImages;
}


function callWStoUpdateImages(listCodeProduct, images) {
    var resultado = -1;
    var namesImages = "";
    for (var image of images.keys()) {
        namesImages += image + ",";
    }
    $.ajax({
        type: "post",
        url: '/Producto/UpdateImages',
        data: { listCodeProduct: listCodeProduct, namesImages: namesImages },
        async: false,
        success: function (result) {
            resultado = result;
        },
        error: function (xhr) {
            alertify.alert("INFORMACIÓN DE ERROR", "Código de error: " + xhr.status +
                ".<br> Mensaje de error: " + xhr.statusText +
                ".<br>Contacte con el departamento de IT si el error persiste. <br> Gracias.");
        }
    });
    return resultado;
}

function uploadImageToTfpServer(imageName) {
   var resultado = -1;
    $.ajax({
        type: "post",
        url: '/Producto/UploadImageToFtpServer',
        data: { imageName: imageName },
        async: false,
        success: function (result) {
            resultado = result.Code;
        },
        error: function (xhr) {
            alertify.alert("INFORMACIÓN DE ERROR", "Código de error: " + xhr.status +
                ".<br> Mensaje de error: " + xhr.statusText +
                ".<br>Contacte con el departamento de IT si el error persiste. <br> Gracias.");
        }
    });
   return resultado;
}

function deleteImagesFromFrpServer(ean) {
    $.ajax({
        type: "get",
        url: '/Producto/DeteleImageFromFtpServer',
        data: { ean: ean },
        async: false // Genera un error por ser ejecutado en modo sincrono pero su funcón de eliminar las fotos antiguas del servidor FTP la realiza correctamente.
    });
}

function checkCodeAndImageProductToUpdate(imgs, cprods) {

    var resultado;
    var namesImages = "";
    for (var image of imgs.keys()) {
        namesImages +=image+",";
    }
    $.ajax({
        type: "post",
        url: '/Producto/CheckCodeAndImageProductToUpdate',
        data: { codesProduct: cprods, namesImage: namesImages },
        async: false,
        success: function (result) {
            resultado = result;
        },
        error: function (xhr) {
            alertify.alert("INFORMACIÓN DE ERROR", "Código de error: " + xhr.status +
                ".<br> Mensaje de error: " + xhr.statusText +
                ".<br>Contacte con el departamento de IT si el error persiste. <br> Gracias.");
        }
    });

    return resultado;
}

function UploadImage() {
    $('#btnUploadImage').on('click', function (e) {
        e.preventDefault();

        $(this).attr("disabled", "disabled");
        var images = getImageSelect();
        var codesProduct = $("#listCodeProduct").val();
        var productNotUpdatedForCheck = "";
        var errorMessages = "";
        var uploadImageInLocalServerCorrectly = false;

        if ($("#listCodeProduct").val().length == 0) {
            $('#listCodeProduct').addClass("is-invalid");
            $("#messageSuccessResultUpdatePhoto").parents(".card").hide();
            $('#messageErrorResultUpdatePhoto').html('Debe instroducir mínimo un código de producto.');
            $('#messageErrorResultUpdatePhoto').parents('.card').show();
            $("#btnUploadImage").removeAttr('disabled');
            return;
        } else if (!/^(([0-9]{5})*(,)?(\s)?(\n)?)*$/.test(($("#listCodeProduct").val()))) {
            $('#listCodeProduct').addClass("is-invalid");
            $("#messageSuccessResultUpdatePhoto").parents(".card").hide();
            $('#messageErrorResultUpdatePhoto').html('Debe instroducir solo códigos númericos de longitud igual a cinco.');
            $('#messageErrorResultUpdatePhoto').parents('.card').show();
            $("#btnUploadImage").removeAttr('disabled');
            return;
        } else if ($("#inputFileImages").get(0).files.length == 0) {
            $("#messageSuccessResultUpdatePhoto").parents(".card").hide();
            $('#messageErrorResultUpdatePhoto').html('Debe seleccionar mínimo una imagene, es requerido.');
            $('#messageErrorResultUpdatePhoto').parents('.card').show();
            $("#btnUploadImage").removeAttr('disabled');
            return;
        } else {
            $("#loadingUpdatePhoto").show();
            $("#loadingUpdatePhoto").children('span').text("Verificando productos...");
            var resultCheck = checkCodeAndImageProductToUpdate(images, codesProduct);

            if (resultCheck.TotalProductNotUpdated == "All") {
                $("#messageSuccessResultUpdatePhoto").parents(".card").hide();
                $('#messageErrorResultUpdatePhoto').html('No existe ningún productos de los introducidos o ninguno tiene el codigo ean coincidente con las imagenes seleccionadas.');
                $('#messageErrorResultUpdatePhoto').parents('.card').show();
                $("#loadingUpdatePhoto").hide();
                $("#btnUploadImage").removeAttr('disabled');
                return;
            } else productNotUpdatedForCheck = resultCheck.TotalProductNotUpdated != "0" ? resultCheck.CodeProducts : "";
        }



       
        $("#loadingUpdatePhoto").children('span').text("Eliminando fotos antiguas y subiendo nuevas fotos al servidor...");
        // buscar la forma de enviar los skus tambíen
        $.ajax({
            type: "POST",
            url: '/Producto/UploadImages',
            contentType: false,
            processData: false,
            data: images,
            success: function (result) {
                if (result.Code == 1) {
                    $("#loadingUpdatePhoto").children('span').text("Subiendo nuevas fotos al servidor FTP...");
                    uploadImageInLocalServerCorrectly = true;
                    var imageNotUpload = "";
                    var totalNotUploaded = 0;
                    var totalImage = 0;

                    for (var image of images.keys()) {
                        totalImage++;
                    }

                    for (var image of images.keys()) {
                        if (uploadImageToTfpServer(image) == 0) {
                            imageNotUpload += image;
                            totalNotUploaded++;
                        }
                    }

                    if (totalNotUploaded == totalImage) {
                        uploadImageInLocalServerCorrectly = false;
                    }

                    if (imageNotUpload != "") {
                        errorMessages += "</b > <br>No se han subido las siguienes imagen/es: <b>" + imageNotUpload + "</b> vuelva a intentar subir estas imagenes.";
                    }

                    

                } else {
                    errorMessages += "<br>Ha ocurrido un error al subir las imagenes en la aplicación local, vualvalo a intentar.";
                }
            },
            error: function (xhr) {
                alertify.alert("INFORMACIÓN DE ERROR", "Código de error: " + xhr.status +
                    ".<br> Mensaje de error: " + xhr.statusText +
                    ".<br>Contacte con el departamento de IT si el error persiste. <br> Gracias.");
            },
            complete: function () {
                if (uploadImageInLocalServerCorrectly) {
                    $("#loadingUpdatePhoto").children('span').text("Ejecutando llamada de WebServices...");
                    var messageResult = callWStoUpdateImages(codesProduct, images);

                    if (messageResult != "")
                        errorMessages += messageResult ;

                    if (productNotUpdatedForCheck != "")
                        errorMessages += "<br>- No se ha podido actualizar los siguientes productos porque no existen o no su código de barra no exsites, esta vacío o no coinciden con ninguna imagen subida: <b>" + productNotUpdatedForCheck + "</b>.";

                }
                else {
                    errorMessages = "<br>No se han actualizado las fotos de cada producto porque ha ocurrido un error y no se ha podido subir ninguna foto al servidor.";
                }

                if (errorMessages != "") {
                    //alertify.errorAlert("<b>ERROR:</b><br>" + errorMessages);

                    $("#messageSuccessResultUpdatePhoto").parents(".card").hide();
                    $('#messageErrorResultUpdatePhoto').html(errorMessages);
                    $('#messageErrorResultUpdatePhoto').parents('.card').show();
                }
                else {
                    $('#messageErrorResultUpdatePhoto').parents('.card').hide();
                    $('#messageSuccessResultUpdatePhoto').html('El proceso de actualización de fotos finalizó correctamente. Verifique las fotos subidas en la web.');
                    $("#messageSuccessResultUpdatePhoto").parents(".card").show();
                }

                $("#loadingUpdatePhoto").hide();
                $("#btnUploadImage").removeAttr('disabled');
            }
        });
        
    });


}

function setHandlerEditFormTextareaUpdatePhotoAndInputFile() {
    $("#listCodeProduct").on("focusin", function () {
        $(this).removeClass("is-invalid");
        $("#messageSuccessResultUpdatePhoto").parents(".card").hide();
        $("#messageErrorResultUpdatePhoto").parents(".card").hide();
    });

    $("#inputFileImages").on("focusin", function () {
        $("#messageSuccessResultUpdatePhoto").parents(".card").hide();
        $("#messageErrorResultUpdatePhoto").parents(".card").hide();
    });
}



    function productEventSet(section) {

        showMessageReceived();
        //UploadImage();
        switch (section) {
            case SECTION_PRODUCT_UPDATE_PHOTO:
                UploadImage();
                setHandlerEditFormTextareaUpdatePhotoAndInputFile();
                break;
            case SECTION_PRODUCT_VIEW:
                $(".edit-product").on('click', function (e) {
                    e.preventDefault();
                    var productButtonManage = $(this);
                    
                    $.ajax({
                        data: { id: productButtonManage.prop('id'), ajaxQuery: true },
                        url: productButtonManage.attr("href"),
                        type: "get",
                        success: function (result, status, xhr) {
                            $("#resultAjax").html(result);
                            productEventSet(SECTION_PRODUCT_EDIT);
                            settingMaxHeightEditSection();
                        },
                        error: function (xhr, status, error) {
                            alertify.errorAlert("Código de error: <b>" + xhr.status + "</b><br> Mensaje de error: <b>" + xhr.statusText + "</b>.<br>Vuelva a <b>intentarlo</b>, contacte con el departamento de IT si el error persiste. <br> Gracias.");
                        }
                    });

                });
                break;
            case SECTION_PRODUCT_EDIT:
                commonFunctionInSectionsCreateAndEdit();
                setHandlerToBtnUpdateDocument();

                $("#btnSubmitEditProduct").on("click", function (e) {
                    e.preventDefault();
                    $("#loadingMessageForCrearUpdateIndividually").show();
                    $("#sku").removeAttr('disabled');
                    $("#id_familia").removeAttr('disabled');
                    var dataProduct = decodeURIComponent($("#formAttributeForNewProduct").serialize());
                    $("#sku").attr('disabled', 'disabled');
                    $("#id_familia").attr('disabled', 'disabled');
                    $.ajax({
                        data: {
                            dataNewProduct: dataProduct
                        },
                        url: "/Producto/Update",
                        type: "post",
                        success: function (result, status, xhr) {
                            $("#messagesSection").html(result);
                            showMessageReceived();
                        },
                        error: function (xhr, status, error) {
                            alertify.alert("INFORMACIÓN DE ERROR", "Código de error: " + xhr.status +
                                ".<br> Mensaje de error: " + xhr.statusText +
                                ".<br>Contacte con el departamento de IT si el error persiste. <br> Gracias.");
                        },
                        complete: function () {
                            $("#loadingMessageForCrearUpdateIndividually").hide();
                        }
                    });

                });
                break;
            case SECTION_PRODUCT_CREATE:
                commonFunctionInSectionsCreateAndEdit();

                $("#inputExcelNewProduct").on("change", function (e) {
                    // add attribute HTML multiple to this element for send several Excels
                    var files = $("#inputExcelNewProduct").get(0).files;

                    for (var a = 0; a < files.length; a++) {
                        if (files[a].size > 1073741824) {
                            alertify.errorAlert("<pre>" + ERROR_FILE_EXCEEDS_LIMIT_STORAGE + "<pre>");
                        }
                    }

                    /*if ($("#inputExcelNewProduct")[0].files[0].size > 1073741824) {
                        alertify.errorAlert("<pre>" + ERROR_FILE_EXCEEDS_LIMIT_STORAGE + "<pre>");
                    }*/
                });

                $("#CreateProductsWithExcel").on("click", function (e) {
                    e.preventDefault();
                    
                    var files = $("#inputExcelNewProduct").get(0).files;
                    if (files.length > 0) {
                        var data = new FormData();
                        for (var a = 0; a < files.length; a++) {
                            data.append(files[a].name, files[a]);
                        }

                        $.ajax({
                            beforeSend: function () { $("#loadingMessageForCreateWithExcel").show(); },
                            data: data,
                            url: "/api/Productos/CreateThroughExcel",
                            type: "post",
                            contentType: false,
                            processData: false,
                            success: function (messages, status, xhr) {
                                if (messages != null) {
                                    var tableMessageSuccess = getTableMessage(messages.messageSuccess, "messageSuccess");

                                    var tableMessageWarning = getTableMessage(messages.messageWarning, "messageWarning");

                                    var tableMessageError = getTableMessage(messages.messageError, "messageError");

                                    $("#messagesSection").html(tableMessageSuccess + tableMessageWarning + tableMessageError);
                                    showMessageReceived(true);
                                    $("#inputExcelNewProduct").val("");

                                }
                                else {
                                    alertify.errorAlert(ERROR_NOT_MESSAGE_RECEIVE);
                                }
                            },
                            error: function (xhr, status, error) {
                                alertify.alert("INFORMACIÓN DE ERROR", "Código de error: " + xhr.status +
                                    ".<br> Mensaje de error: " + xhr.statusText +
                                    ".<br>Contacte con el departamento de IT si el error persiste. <br> Gracias.");
                            },
                            complete: function () {
                                $("#loadingMessageForCreateWithExcel").hide();
                            }
                        });
                    } else {
                        alertify.errorAlert(ERROR_FILE_NOT_SENT);
                    }

                });


                $("#btnSubmitCreateProductIndividually").on("click", function (e) {
                    e.preventDefault();
                    $("#loadingMessageForCrearUpdateIndividually").show();
                    $.ajax({
                        data: {
                            dataNewProduct: decodeURIComponent($("#formAttributeForNewProduct").serialize())
                        },
                        url: "/Producto/Create",
                        type: "post",
                        success: function (result, status, xhr) {
                            $("#messagesSection").html(result);
                            showMessageReceived();
                            $("#formAttributeForNewProduct")[0].reset();
                            $("#formAttributeForNewProduct").find("select").prop("selectedIndex", 0);
                        },
                        error: function (xhr, status, error) {
                            alertify.alert("INFORMACIÓN DE ERROR", "Código de error: " + xhr.status +
                                ".<br> Mensaje de error: " + xhr.statusText +
                                ".<br>Contacte con el departamento de IT si el error persiste. <br> Gracias.");
                        },
                        complete: function () {
                            $("#loadingMessageForCrearUpdateIndividually").hide();
                        }
                    });
                });

                $("#id_familia").on("change", function () {
                    var id_familia = $(this).find(':selected').val();

                    $.ajax({
                        data: { "idFamily": id_familia },
                        url: "/Producto/AttributesProduct",
                        type: "get",
                        success: function (result, status, xhr) {
                            $("#attribute-input-container").empty().append(result);
                        },
                        error: function (xhr, status, error) {
                            alertify.alert("INFORMACIÓN DE ERROR", "Código de error: " + xhr.status +
                                ".<br> Mensaje de error: " + xhr.statusText +
                                ".<br>Contacte con el departamento de IT si el error persiste. <br> Gracias.");
                        }
                    });
                });
                break;
        }

        
        

        


       

        









   



    }