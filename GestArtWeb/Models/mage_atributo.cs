//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GestArtWeb.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class mage_atributo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public mage_atributo()
        {
            this.mage_articulo_atributo_valor = new HashSet<mage_articulo_atributo_valor>();
            this.mage_atributo_opcion = new HashSet<mage_atributo_opcion>();
            this.mage_familia = new HashSet<mage_familia>();
        }
    
        public short id_atributo { get; set; }
        public string codigo { get; set; }
        public string nombre { get; set; }
        public string tipo { get; set; }
        public Nullable<short> filtrable { get; set; }
        public Nullable<short> es_visible { get; set; }
        public Nullable<int> grupo_id { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<mage_articulo_atributo_valor> mage_articulo_atributo_valor { get; set; }
        public virtual mage_atributo_grupo mage_atributo_grupo { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<mage_atributo_opcion> mage_atributo_opcion { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<mage_familia> mage_familia { get; set; }
    }
}
