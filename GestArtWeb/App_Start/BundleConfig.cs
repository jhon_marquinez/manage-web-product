﻿using System.Web;
using System.Web.Optimization;

namespace GestArtWeb
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/bootstrap.js",
                        "~/Scripts/alertify.js",
                        "~/scripts/bootstrap-treeview.min.js",
                        "~/scripts/slinky.min.js",
                        //"~/Scripts/site.js",
                        "~/Scripts/category.js",
                        "~/Scripts/product.js",
                        "~/Scripts/site.js",
                        "~/scripts/typeahead.bundle.js",
                        "~/scripts/jquery-ui.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            /*bundles.Add(new ScriptBundle("~/bundles/alertify").Include(
                        "~/Scripts/alertify.js"));*/

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap-spacelab.css",
                      "~/Content/typeahead.css",
                      "~/Content/site.css",
                      "~/Content/alertify/themes/bootstrap.ccs",
                      "~/Content/alertify/alertify.rtl.css",
                      "~/Content/alertify/themes/default.rtl.css",
                      "~/Content/jquery-ui/jquery-ui.css",
                      "~/Content/bootstrap-treeview.min.css",
                      "~/Content/slinky/slinky.min.css"
                      ));

            
        }
    }
}
