﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using GestArtWeb.Models;
using GestArtWeb.Dtos;

namespace GestArtWeb.App_Start
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            Mapper.CreateMap<mage_categoria, CategoriaDto>();
            Mapper.CreateMap<CategoriaDto, mage_categoria>();

            Mapper.CreateMap<mage_articulo, ProductoDto>();
            Mapper.CreateMap<ProductoDto, mage_articulo>();

            Mapper.CreateMap<mage_articulo_atributo_valor, AtributoDeProductoDto>();
            Mapper.CreateMap<AtributoDeProductoDto, mage_articulo_atributo_valor>();
        }
    }
}