﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace GestArtWeb.Helper
{
    public class Parameter
    {

        #region Properties

        public string sParameterName;
        private SqlDbType ParameterTypeSql;
        private MySqlDbType ParameterTypeMySql;
        private int iParameterSize;
        private int iParameterPrecision;
        private int iParameterScale;
        private object oParameterValue;

        #endregion

        #region Constructors

        public Parameter(string name, SqlDbType type, int size, int precision, int scale, object value)
        {
            this.sParameterName = name;
            this.ParameterTypeSql = type;
            this.iParameterSize = size;
            this.iParameterPrecision = precision;
            this.iParameterScale = scale;
            this.oParameterValue = value;
        }

        public Parameter(string name, MySqlDbType type, int size, int precision, int scale, object value)
        {
            this.sParameterName = name;
            this.ParameterTypeMySql = type;
            this.iParameterSize = size;
            this.iParameterPrecision = precision;
            this.iParameterScale = scale;
            this.oParameterValue = value;
        }

        #endregion

        #region Methods

        public static SqlParameter[] CreateParametersSql(List<Parameter> listParameter)
        {
            int i = 0;
            SqlParameter[] sqlParrameters = new SqlParameter[listParameter.Count];
            foreach (var param in listParameter)
            {
                sqlParrameters[i] = new SqlParameter();
                sqlParrameters[i].ParameterName = param.sParameterName;
                sqlParrameters[i].SqlDbType = param.ParameterTypeSql;
                sqlParrameters[i].Size = param.iParameterSize;
                sqlParrameters[i].Precision = Convert.ToByte(param.iParameterPrecision);
                sqlParrameters[i].Scale = Convert.ToByte(param.iParameterScale);
                sqlParrameters[i].SqlValue = param.oParameterValue;
                i++;

            }
            //listParameter.Clear();
            return sqlParrameters;

        }

        public static MySqlParameter[] CreateParametersMysql(List<Parameter> listParameter)
        {
            int i = 0;
            MySqlParameter[] sqlParameters = new MySqlParameter[listParameter.Count];
            foreach (var param in listParameter)
            {
                sqlParameters[i] = new MySqlParameter();
                sqlParameters[i].ParameterName = param.sParameterName;
                sqlParameters[i].MySqlDbType = param.ParameterTypeMySql;
                sqlParameters[i].Size = param.iParameterSize;
                sqlParameters[i].Precision = Convert.ToByte(param.iParameterPrecision);
                sqlParameters[i].Scale = Convert.ToByte(param.iParameterScale);
                sqlParameters[i].Value = param.oParameterValue;
                i++;

            }
            //listParameter.Clear();
            return sqlParameters;

        }

        #endregion


    }
}