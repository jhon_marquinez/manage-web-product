﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestArtWeb.Helper
{
    public class QueryTransaction
    {
        #region Properties
        private string sql;
        private MySqlParameter[] aParametersMySQLServer;
        #endregion
        #region Construct
        public QueryTransaction() { }
        public QueryTransaction(string sql, MySqlParameter[] aParametersMySQLServer)
        {
            this.sql = sql;
            this.aParametersMySQLServer = aParametersMySQLServer;
        }
        #endregion
        #region Setters and Getters
        public string Sql
        {
            get { return sql; }
            set { sql = value; }
        }
        public MySqlParameter[] ParametersMySQLServer
        {
            get { return aParametersMySQLServer; }
            set { aParametersMySQLServer = value; }
        }
        #endregion

    }
}