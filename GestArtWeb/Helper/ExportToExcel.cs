﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;
using GestArtWeb.Models;

namespace GestArtWeb.Helper
{
    public class ExportToExcel
    {
        public static string ExcelContentType
        {
            get
            { return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"; }
        }

        public static DataTable ListToDataTable<T>(List<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable dataTable = new DataTable();

            for (int i = 0; i < properties.Count; i++)
            {
                PropertyDescriptor property = properties[i];
                dataTable.Columns.Add(property.Name, Nullable.GetUnderlyingType(property.PropertyType) ?? property.PropertyType);
            }

            object[] values = new object[properties.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = properties[i].GetValue(item);
                }

                dataTable.Rows.Add(values);
            }
            return dataTable;
        }


        public static DataTable ListCategoriesoDataTable(List<mage_categoria> data)
        {

            DataTable dataTable = new DataTable();

            dataTable.Columns.Add("ID 1");
            dataTable.Columns.Add("CATEGORÍA 1");
            dataTable.Columns.Add("ID 2");
            dataTable.Columns.Add("CATEGORÍA 2");
            dataTable.Columns.Add("ID 3");
            dataTable.Columns.Add("CATEGORÍA 3");

            object[] values = new object[6];
            foreach (mage_categoria cat in data)
            {

                switch (cat.nivel)
                {
                    case 2:
                        values[0] = cat.entity_id;
                        values[1] = cat.nombre;
                        break;
                    case 3:
                        values[2] = cat.entity_id;
                        values[3] = cat.nombre;
                        values[0] = cat.mage_categoria2.entity_id;
                        values[1] = cat.mage_categoria2.nombre;
                        break;
                    case 4:
                        values[4] = cat.entity_id;
                        values[5] = cat.nombre;
                        values[2] = cat.mage_categoria2.entity_id;
                        values[3] = cat.mage_categoria2.nombre;
                        values[0] = cat.mage_categoria2.mage_categoria2.entity_id;
                        values[1] = cat.mage_categoria2.mage_categoria2.nombre;
                        break;
                }

                dataTable.Rows.Add(values);
            }
            return dataTable;
        }

        public static byte[] ExportExcel(DataTable dataTable, string heading, string[ , ] columnsToTakeToRename)
        {

            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                ExcelWorksheet workSheet = package.Workbook.Worksheets.Add(String.Format("Datos de {0}", heading));
                int startRowFrom = String.IsNullOrEmpty(heading) ? 1 : 3;

                if (columnsToTakeToRename.Length > 0)
                {
                    // change name of column header
                    for (int i = dataTable.Columns.Count - 1; i >= 0; i--)
                    {
                        int t = columnsToTakeToRename.Length / columnsToTakeToRename.Rank;
                        for (int j = 0; j < t; j++)
                        {
                            if (columnsToTakeToRename[j, 0] == dataTable.Columns[i].ColumnName)
                            {
                                dataTable.Columns[i].ColumnName = columnsToTakeToRename[j, 1];
                                break;
                            }
                        }
                    }
                }

                // add the content into the Excel file  
                workSheet.Cells["A" + startRowFrom].LoadFromDataTable(dataTable, true);

                // autofit width of cells with small content  
                int columnIndex = 1;
                foreach (DataColumn column in dataTable.Columns)
                {
                    ExcelRange columnCells = workSheet.Cells[workSheet.Dimension.Start.Row, columnIndex, workSheet.Dimension.End.Row, columnIndex];
                    int maxLength = 0;
                    try
                    {
                        maxLength = columnCells.Max(cell => cell.Value.ToString().Count());
                    }
                    catch (Exception ex)
                    {
                        maxLength = 149;
                    }

                    if (maxLength < 150)
                    {
                        workSheet.Column(columnIndex).AutoFit();
                    }

                    columnIndex++;
                }

                // format header - bold, yellow on black  
                using (ExcelRange r = workSheet.Cells[startRowFrom, 1, startRowFrom, dataTable.Columns.Count])
                {
                    r.Style.Font.Color.SetColor(System.Drawing.Color.White);
                    r.Style.Font.Bold = true;
                    r.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    r.Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#000000"));
                }

                // format cells - add borders  
                using (ExcelRange r = workSheet.Cells[startRowFrom + 1, 1, startRowFrom + dataTable.Rows.Count, dataTable.Columns.Count])
                {
                    r.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    r.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    r.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    r.Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    r.Style.Border.Top.Color.SetColor(System.Drawing.Color.Black);
                    r.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                    r.Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                    r.Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                }

                if (columnsToTakeToRename.Length > 0)
                {
                    // removed ignored columns  
                    for (int i = dataTable.Columns.Count - 1; i >= 0; i--)
                    {
                        int t = columnsToTakeToRename.Length / columnsToTakeToRename.Rank;
                        bool foundColumn = false;
                        for (int j = 0; j < t; j++)
                        {
                            if (columnsToTakeToRename[j, 1] == dataTable.Columns[i].ColumnName)
                            {
                                foundColumn = true;
                                break;
                            }
                        }
                        if (!foundColumn)
                        {
                            workSheet.DeleteColumn(i + 1);
                        }
                    }
                }
                

                if (!String.IsNullOrEmpty(heading))
                {
                    workSheet.Cells["A1"].Value = heading;
                    workSheet.Cells["A1"].Style.Font.Size = 20;

                    workSheet.InsertColumn(1, 1);
                    workSheet.InsertRow(1, 1);
                    workSheet.Column(1).Width = 5;
                }

                result = package.GetAsByteArray();
            }

            return result;
        }

        public static byte[] ExportExcel<T>(List<T> data, string Heading, string[ , ] ColumnsToTake)
        {
            return ExportExcel(ListToDataTable<T>(data), Heading, ColumnsToTake);
        }

    }
}  
  