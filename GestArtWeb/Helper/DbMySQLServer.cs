﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Configuration;

namespace GestArtWeb.Helper
{
    public class DbMySQLServer
    {

        private string connectionString;
        private MySqlConnection connection;
        private MySqlCommand command;
        private MySqlDataReader reader;
        private MySqlTransaction transaction;
        private MySqlDataAdapter adapter;
        private DataTable dataTable;


        //Constructor
        public DbMySQLServer()
        {
            Initialize();
        }

        private void Initialize()
        {
            connectionString = ConfigurationManager.ConnectionStrings["MagentoData"].ConnectionString;
        }


        /// <summary>
        /// Open connection with Mysql (DB of Magento).
        /// </summary>
        private bool OpenConnection()
        {
            try
            {
                connection = new MySqlConnection(connectionString);
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message); 
            }
        }

        /// <summary>
        /// Close connection.
        /// </summary>
        private bool CloseConnection()
        {
            bool closed = false;
            if (connection != null)
            {
                try
                {
                    connection.Close();
                    connection.Dispose();
                    connection = null;
                    closed = true;
                }
                catch (MySqlException ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            return closed;
        }

        /// <summary>
        /// Reset the instance objects named connection, command, reader and transaction.
        /// </summary>
        public void Dispose()
        {
            if (connection != null) { connection.Close(); connection.Dispose(); connection = null; }
            if (command != null) { command.Dispose(); command = null; }
            if (reader != null) { reader.Close(); reader.Dispose(); reader = null; }
            if (transaction != null) { transaction.Dispose(); transaction = null; }
        }

        /// <summary>
        /// Reset instance object command.
        /// </summary>
        private void DisposeCommand()
        {
            if (command != null) { command.Dispose(); command = null; }
        }


        
        public void ExecuteProcedure(string procedureName, MySqlParameter[] parameters = null)
        {
            try
            {
                OpenConnection();
                command = new MySqlCommand(procedureName, connection);
                command.CommandType = CommandType.StoredProcedure;
                if (parameters != null) { command.Parameters.AddRange(parameters); }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection();
                DisposeCommand();
            }
        }


        /// <summary>
        /// Use to execute sentence Select in MySQL.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public MySqlDataReader ExecuteQuery(string query, MySqlParameter[] parameters = null)
        {
            reader = null;
            try
            {
                OpenConnection();
                command = new MySqlCommand(query, connection);
                if (parameters != null) command.Parameters.AddRange(parameters);
                reader = command.ExecuteReader();
                command.Parameters.Clear();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return reader;
        }

        /// <summary>
        /// Execute query and return simple object.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameters"></param>
        /// <returns>object</returns>
        public object ExecuteSimpleQuery(string query, MySqlParameter[] parameters = null)
        {
            object retorno = null;
            try
            {
                OpenConnection();
                command = new MySqlCommand() { CommandText = query, Connection = connection };
                if (parameters != null) command.Parameters.AddRange(parameters);
                reader = command.ExecuteReader();
                if (reader.Read()) retorno = reader.GetValue(0);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection();
                DisposeCommand();
            }

            return retorno;

        }



        //Update, delete or insert statement
        /// <summary>
        /// Executes sentences as update, delete and insert.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public int ExecuteSentenceCommand(string query, MySqlParameter[] parameters)
        {
            int numberRowsAffected = 0;
            try
            {
                OpenConnection();
                command = new MySqlCommand() { CommandText = query, Connection = connection };
                if (parameters != null){ command.Parameters.AddRange(parameters); }
                
                numberRowsAffected = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection();
                DisposeCommand();
            }

            return numberRowsAffected;
        }


        /// <summary>
        /// Executes query and returns de values inside a DataTable object.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameters"></param>
        /// <returns>DataTable</returns>
        public DataTable LoadDataTable(string query, MySqlParameter[] parameters = null)
        {
            dataTable = null;
            try
            {
                dataTable = new DataTable();
                OpenConnection();
                command = new MySqlCommand() { CommandText = query, Connection = connection };
                if (parameters != null) command.Parameters.AddRange(parameters);

                adapter = new MySqlDataAdapter(command);
                adapter.Fill(dataTable);
                command.Parameters.Clear();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection();
                DisposeCommand();
            }

            return dataTable;
        }


        /// <summary>
        /// This method executes a MySQL transaction.
        /// </summary>
        /// <param name="queriesToTransaction"></param>
        /// <returns></returns>
        public bool ExecuteTransaction(List<QueryTransaction> queriesToTransaction)
        {
            bool success = false;
            try
            {
                OpenConnection();
                transaction = connection.BeginTransaction();
                command = new MySqlCommand();
                command.Connection = connection;
                command.Transaction = transaction;
                foreach (var queryTrans in queriesToTransaction)
                {
                    command.CommandText = queryTrans.Sql;
                    if (queryTrans.ParametersMySQLServer != null) command.Parameters.AddRange(queryTrans.ParametersMySQLServer);
                    command.ExecuteNonQuery();
                    command.Parameters.Clear();
                }
                transaction.Commit();
                success = true;
            }
            catch (Exception e)
            {
                try
                {
                    transaction.Rollback();
                }
                catch (Exception ex)
                {
                    if (transaction.Connection != null) throw new Exception(ex.Message);
                }
                success = success == false ? success : false;
            }
            finally
            {
                CloseConnection();
                DisposeCommand();
            }

            return success;
        }



    }
}