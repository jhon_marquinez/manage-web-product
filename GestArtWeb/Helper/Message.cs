﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestArtWeb.Helper
{
    public class Message
    {
        public const string ERROR_TYPE = "messageError";
        public const string WARNING_TYPE = "messageWarning";
        public const string SUCCESS_TYPE = "messageSuccess";
        public const string INFO_TYPE = "messageInfo";
        public const string QUESTION_TYPE = "messageQuestion";
        public string code { get; private set; }
        public string message { get; private set; }
        public string btnAction { get; private set; }
        public const string ERROR_FILE_NOT_SENT = "<b>ATENCIÓN:</b> <br>Es requerido que antes de enviar el fichero Excel primero debe de seleccionarlo. <br><br>SOLUCIÓN: <br>1) Ha de dar clic en el botón de <b>Seleccionar archivo</b> y elegir un fichero excel almacenado en algún dispositivo de almacenamiento conectado al sistema <i>(recuerde que los ficheros excel son de extención <b>.xls</b> o <b>.xlsx</b>)<i>.";
        public const string ERROR_FILE_NOT_TYPE_EXCEL = "<strong>ATENCIÓN:</strong> <br>El fichero enviado no puede ser de un tipo distinto a Excel. <br><br>SOLUCIÓN: <br>1) Intente enviar de nuevo un fichero excel <i>(recuerde que los ficheros excel son de extención <b>.xls</b> o <b>.xlsx</b>)<i>.";
        public const string ERROR_COLUMN_NOT_MATCH_WITH_ANY_ATTRIBUTE = "<b>ATENCIÓN:</b> <br>Cada columna tiene exactamente el mismo nombre de algún atributo de la web. <br><br>La columna de denominada <strong>'{0}'</strong> no pertenece al conjunto de atributos de la web. <br><br>SOLUCIÓN: <br>1) Crear un atributo exactemente con el mismo nombre de la columna en este caso el nombre sería {1}. <br>2) Quitar la columna del Excel. <br>";
        public const string ERROR_INSERT_DATA_ON_DATABASE = "<b>ATENCIÓN:</b> <br>Se ha producido un error al insertar la información en la base de datos de la web <i>(Magento)</i> o en la base de datos de Alfa Dyser.<br><br>DETALLES: <br> 1) Código de articulo: <b>{0}</b><br> 2) Atributo: <b>{1}</b><br> 3) Valor de atributo: <b>{2}</b>";
        public const string ERROR_CALL_PROCEDURE_NEW_UPDATE_PRODUCT = "Se ha producido un error al ejecutar el procedimiento almacenado que la base de datos de Magento.<br><br> DETALLES:<br> Este procedimiento tiene la función de insertar o actualizar los datos de las tablas int, decimal, varchar y text con los datos informados del/los producto/os almacenado en la tabla de importación llamada <b>ad_new_update_product</b>. <br>Nombre del procedimiento: <b>ad_add_new_update_product</b>";
        public const string WARNING_FAMILY_NOT_ADDED = "<b>ATENCIÓN:</b> <br>No se ha añadido/actualizado la familia del producto con código {0}. <br><br><b>CONSECUENCIAS:</b> <br>Los clientes cuando vean la ficha de este producto puede darse el caso de no ver todos sus atributos, la ficha estará incompleta. <br><br><b>RAZÓN:</b> <br>No se ha podido añadir la familia al producto con código {1} porque no se ha informado en nuestro sistema o la familia informada en nuestro sistema es <b>Herramientas</b> y la <b>cat1</b> informada es distinta a las categorías Herramientas Eléctricas(5628) y Herramientas Manual(5629). <br><br><b>SOLUCIÓN:</b> <br>Con unas de las herramientas de administración como el <b>'Project 4'</b> intente asignar una familia y desde este software intente <b>actualizar la familia</b>";
        public const string ERROR_CREATE_PRODUCT_AND_INSERT_FAMILY = "<b>ATENCIÓN:</b> <br>Ha ocurrido un error al crear el producto <b>{0}</b> o al insertar su familia en la web.<br><br> <b>RECOMENDACIÓN:</b> <br>Intentelo de nuevo.";
        public const string ERROR_INSERT_DATA_COMMON_IN_WEB = "<b>ATENCIÓN:</b> <br>Ha ocurrido un error al insertar los datos comunes del producto <b>{0}</b> en la web.<br><br> <b>RECOMENDACIÓN:</b> <br>Intentelo de nuevo.";
        public const string ERROR_UPDATE_ENTITY_ID_PRODUCTS_IN_ALFA = "<b>ATENCIÓN:</b> <br>Ha ocurrido un error al intentar actualizar la <b>ID de entidad</b> de los productos que no tienen esta identificación informada en la base de datos de Alfa Dyser.<br><br><b>INFORMACIÓN:</b><br>La ID de entidad es la identificación que utiliza Magento para los productos en la web, la cual es necesaria tener informada en la base de datos local de Alfa Dyser. <br><br><b>RECOMENDACIÓN:</b><br>Intente asignar la ID de entidad de todos los productos en la base de datos local de Alfa Dyser que todavía no tienen dando clic en el botón <b>Actualizar IDs Entidades</b>.";
        public const string ERRO_EXCEL_NOT_CONTAIN_NO_ROW_DATA = "<b>ATENCIÓN:</b> <br>Se ha producido un error, el motivo es porque el Excel pasado no contiene ninguna fila con datos.";
        public const string ERRO_UPLOAD_DATA_PRODUCTS_IN_TEMPORARY_TABLE_ON_MAGENTO = "<b>ATENCIÓN:</b> <br>Se ha producido un error, la causa ha sido al subir de productos en una tabla temporal de Magento para posteriormente reinsertar esa información a la estructura de tablas de Magento.<br><br> <b>RECOMENDACIÓN:</b> <br>Intentelo de nuevo.";
        public const string ERROR_UPLOAD_ATTRIBUTES_OF_PRODUCT = "<b>ATENCIÓN:</b> <br>No se han podido subir los atributos del producto con código <b>{0}</b> en la tabla temporal de Magento para su posterior reubicación. <br><br><b>INFORMACIÓN:</b> <br>Ocurrió un error al subir el atributo llamado: <b>{1}</b>. <br><br><b>CONSECUENCIAS:</b> <br>El producto no será creado o actualizado. <br><br><b>RECOMENDACIÓN:</b><br>Intente volver a subir los atributos del producto dando clic en el botón <b>Reintentar</b>.";
        public const string ERROR_UPLOAD_FAMILY_OF_PRODUCT = "<b>ATENCIÓN:</b> <br>No se han podido subir la familia {1} - {2} del producto con código <b/>{0}</b> en la tabla temporal de Magento para su posterior insercción o actualización. <br><br><b>INFORMACIÓN:</b><br> El código de la familia del producto es subida a Magento y es requerido. <br><br><b>CONSECUENCIAS:</b> <br>Sino se sube el identificador de la familia <i>({1})</i> del producto <b>{0}</b>, cuando los clientes accedan a la ficha del producto no verán todos los atributos. <br><br><b>RECOMENDACIÓN:</b><br>Intente volver a subir los atributos del producto dando clic en el botón <b>Reintentar</b>.";
        public const string ERROR_UPLOAD_CATEGORIES_OF_PRODUCT = "<b>ATENCIÓN:</b> <br>No se han podido subir las categoría <b>{1}</b> - <b>{2}</b> del producto con código <b>{0}</b> en la tabla temporal de Magento para su posterior insercción o actualización. <br><br><b>RECOMENDACIÓN:</b><br>Intente volver a subir los atributos del producto dando clic en el botón <b>Reintentar</b>.";
        public const string ERROR_PRODUCT_NOT_EXIST_IN_G733 = "<b>ATENCIÓN:</b> <br>No se ha creado o actualziado el articulo <b>{0}</b> porque no existe en la base de datos de Alfa Dyser. <br><br><b>RECOMENDACIÓN:</b><br>Antes de intentar crear el producto en la web, ha de darlo de alta en Alfa Dyser.";
        public const string ERROR_PRODUCT_ALREADY_EXIST_IN_G733 = "<b>ATENCIÓN:</b> <br>No se ha creado el articulo {0} porque ya existe en la web. <br><br><b>RECOMENDACIÓN:</b><br>Si quiere modificarlo ha de editar el producto.";
        public const string ERROR_PRODUCT_NOT_ALREADY_EXIST_IN_G733 = "<b>ATENCIÓN:</b> <br>No se ha actualizado el articulo {0} porque no existe en la web. <br><br><b>RECOMENDACIÓN:</b><br>Ha de crear el producto para posteriormente poder modificarlo.";
        public const string ERROR_PRODUCT_CODE_CANT_BE_EMPTY = "<b>ATENCIÓN:</b> <br>El Código del producto no puede estar vacío.";
        public const string ERROR_SEND_ONLY_CODE_PRODUCT = "<b>ATENCIÓN:</b> <br>Se ha detectado que el código del producto esta vacío o hay diferentes código de producto, se ha de enviar un único código de producto.";
        public const string SUCCESS_PRODUCT_ADDED = "<b>ATENCIÓN:</b> <br>Se ha creado o actualizado el producto satisfactoriamente. <br>Si se lo notifica algún mensaje de advertencia o error posterior o anterior a este, prestele atencíon e intente solventarlo";
        public const string ERROR_PRODUCT_NOT_ADDED = "<b>ATENCIÓN:</b> <br>No se ha creado o actualizado el/los producto/os correctamente.";
        public const string ERROR_PRODUCT_NOT_ADDED_BECAUSE_THERE_IS_SOME_DATA_INCORRECTE = "<b>ATENCIÓN:</b> <br>Se ha producido un error al añadir las categorias, actualizar datos comunes, añadir los atributos del producto <b>{0}</b> en la base de datos de Alfa Dyser. <br><br><b>RECOMENDACIÓN:</b><br> Verifique que los códigos de categorías sean los correctos y los demas datos tambíen y vuelvo a intentar.";
        public const string WARNING_ATTRIBUTE_FOR_PRODUCT_NOT_EXIST = "<b>ATENCIÓN:</b> <br>El atributo con el código <b>{0}</b> para el producto <b>{1}</b> no existe, por lo tanto no se le añadirá al producto.";
        public const string WARNING_FAMILY_INCORRECT_FOR_PRODUCT = "<b>ATENCIÓN:</b> <br>Ha introducido una familia para este producto diferente a la asignada en la base de datos de Alfa Dyser.<br><br><b>INFORMACIÓN:</b><br>Los atributos para los productos son en función de la familia de dichos productos, si usted introduce valores para atributos que no pertenecen a la familia de un producto determinado, dichos atributos no serán visibles en la ficha del producto.<br><br><b>CONSECUENCIAS:</b> <br>La familia por defecto asignada al producto, es la asignada en la base de datos de Alfa Dyser, no la que ha sido enviada. Algunos de los atributos introducidos y enviados no serán visible en la ficha del producto.<br><br><b>RECOMENDACIÓN:</b><br>Edite el producto creado o que va ser creado seleccionando su familia correcta. <br>La familia seleccionada es <b>{0}</b> y la familia asignada es <b>{1}</b>";
        public const string ERROR_CATEGORY_CODE_PARENT = "El código identificativo del padre contiene caracteres no numéricos o el código no existe porque es demasiado grande. El código de la categoría padre solo debe de contener números.";
        public const string ERROR_CATEGORY_TRY_GET_NEW_ENTITY_ID = "No se ha podido obtener el nuevo código identificativo de la categoría. Intente crear de nuevo esta categoría.";
        public const string ERROR_CATEGORY_NOT_INSERT_ON_MAGENTO ="No se ha podido insertar todos datos de la categoría en la base de datos de la web, intentelo de nuevo.";
        public const string ERROR_CATEGORY_ALREADY_EXISTS ="La categoría ya exsite, si no esta subida en la web, ha de editar cambiando algún dato, de esta forma se subirá automáticamente.";
        public const string ERROR_CATEGORY_ALL_DATA_REQUIRED ="Todos los datos son requeridos.";
        public const string ERROR_CATEGORY_PARENT_NOT_EXISTS = "El código identificativo del padre que ha sido introducido no hace referencia a ninguna categoría. Compruebe el código del padre y intente de nuevo.";
        public const string ERROR_CATEGORY_PARENT_WITH_LEVEL_THREE = "La categoría no puede tener un padre que sea de categoría nivel 3";
        public const string ERROR_CATEGORY_NOT_CREATED_IN_ALFA = "No se ha podido añadir la nueva categoría a la base de datos local de Alfa Dyser";
        public const string ERROR_CATEGORY_CREATE_IN_ALFA_BUT_NOT_UPLOAD_ON_MAGENTO = "<br><br>Se ha creado/actualizado la categoría en la base de datos de Alfa pero no se ha subido a la web, ha de editar esta catagoría <b>({0})</b> modificando alguno de sus datos para que se suba a la web o para intentar crear de nuevo ha de contactar con el departamento de IT para eliminar de la base de datos local de Alfa.";
        public const string ERROR_CATEGORY_ROLLBACK_CREATE_ON_MAGENTO = "Se ha creado/actualizado la categoría y puede ser que no se haya añadido información incompleta de la categoría en la web, verifique esta información y borrela manualmente desde el panel de Magento.";
        public const string ERROR_CATEGORY_ID_NOT_EXISTS = "El código de categía <b>{0}</b> no hace referencia a ninguna categoría existente, introduzca ID de cateogría correcto.";
        public const string SUCCESS_CATEGORY_ADDED = "La categoría se ha creado o actualizado correctamente, su codigo identificativo es <b>{0}</b>, su nombre es <b>{1}</b>, su padre es la categoria <i>({2})</i>-{3}. <br><br><b>IMPORTANTE: ha de reindexar la web accediento al panel de control de Magento.</b>";
        public const string ERROR_CATEGORY_UPDATE_IN_ALFA = "No se ha podido actualizar la categoría en la base de datos local de Alfa o intentado actualizar la categoría sin cambiar ninguno de sus datos.";

        public Message(string code, string message)
        {
            this.code = code;
            this.message = message;
        }

        public Message(string code, string message, string btnAction) : this(code, message)
        {
            this.btnAction = btnAction;
        }

        public static string GetBtnAction(string id, string action, string nameToShow)
        {
            return String.Format("<button class=\"btn btn-default messageAction\" data-id=\"{0}\" data-action=\"{1}\">{2}</button>", id, action, nameToShow);
        }

    }
}