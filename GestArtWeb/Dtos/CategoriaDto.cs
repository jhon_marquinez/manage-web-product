﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GestArtWeb.Models;
using System.ComponentModel.DataAnnotations;

namespace GestArtWeb.Dtos
{
    public class CategoriaDto
    {
        public int entity_id { get; set; }
        [Required]
        public string nombre { get; set; }
        public Nullable<int> padre { get; set; }
        public Nullable<int> es_activa { get; set; }
        public Nullable<int> es_anchor { get; set; }
        public Nullable<int> incluida_en_menu { get; set; }
        public Nullable<int> nivel { get; set; }
        public Nullable<int> familia_id { get; set; }
        public int modificado { get; set; }
    }
}