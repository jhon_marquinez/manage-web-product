﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GestArtWeb.Models;

namespace GestArtWeb.Dtos
{
    public class ProductoDto
    {
        public Nullable<int> entity_id { get; set; }
        public Nullable<short> id_familia { get; set; }
        public string sku { get; set; }
        public Nullable<int> modificado { get; set; }
        public ICollection<CategoriaDto> mage_categoria { get; set; }
        public ICollection<AtributoDeProductoDto> mage_articulo_atributo_valor { get; set; }
    }
}