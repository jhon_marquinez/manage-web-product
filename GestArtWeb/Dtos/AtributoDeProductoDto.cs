﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestArtWeb.Dtos
{
    public class AtributoDeProductoDto
    {
        public string sku { get; set; }
        public short id_atributo { get; set; }
        public Nullable<int> value_int { get; set; }
        public Nullable<decimal> value_decimal { get; set; }
        public string value_varchar { get; set; }
        public string value_text { get; set; }
    }
}