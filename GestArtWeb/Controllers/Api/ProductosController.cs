﻿using GestArtWeb.Models;
using GestArtWeb.Helper;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using GestArtWeb.Dtos;
using AutoMapper;
using System;
using System.Web;
using System.Threading.Tasks;
using System.IO;
using Excel;
using System.Data;

namespace GestArtWeb.Controllers.Api
{
    public class ProductosController : ApiController
    {
        private GestArtWebData db;

        public ProductosController()
        {
            db = new GestArtWebData();
        }

        // GET /api/productos
        public IHttpActionResult GetProductos(string skuOrName = null)
        {
            IQueryable<mage_articulo> productsQuery;

            if (!String.IsNullOrWhiteSpace(skuOrName))
                productsQuery = db.mage_articulo.Where(p => p.sku.Length == 5 && (p.sku.Contains(skuOrName) || p.mage_articulo_atributo_valor.FirstOrDefault(a => a.id_atributo == 71).value_varchar.Contains(skuOrName))).OrderBy(p => p.sku);
            else productsQuery = db.mage_articulo.OrderBy(p => p.sku);

            var productsDtos = productsQuery.ToList().Select(Mapper.Map<mage_articulo, ProductoDto>);
            return Ok(productsDtos);
        }

        [HttpPost]
        public async Task<IHttpActionResult> CreateThroughExcel()
        {
            Dictionary<string, List<Message>> MessagesForUsuer = null;
            HttpFileCollection Excels = HttpContext.Current.Request.Files;
            if (Excels.Count > 0) {
                for (var i = 0; i < Excels.Count; i++)
                {
                    HttpPostedFile excel = Excels[i];
                    HttpPostedFileBase excelNewProduct = new HttpPostedFileWrapper(excel);
                    ProductoController ProductController = new ProductoController();

                    //var table = ProductController.LoadExcelInDataTable(excelNewProduct);
                    var table = LoadExcelInDataTable(excelNewProduct);

                    if (table.ContainsKey("excel"))
                    {
                        MessagesForUsuer = await ProductController.CreateOrUpdateProductThroughDataTable((DataTable)table["excel"]);
                    }
                    else
                    {
                        MessagesForUsuer = new Dictionary<string, List<Message>>();
                        MessagesForUsuer.Add(Message.ERROR_TYPE, new List<Message> { new Message("", (string)table[Message.ERROR_TYPE]) });
                    }
                }
            }
            else
            {
                MessagesForUsuer = new Dictionary<string, List<Message>>();
                MessagesForUsuer.Add(Message.ERROR_TYPE, new List<Message> { new Message("", Message.ERROR_FILE_NOT_SENT) });
            }
            return Ok(MessagesForUsuer);
        }

        private Dictionary<string, object> LoadExcelInDataTable(HttpPostedFileBase uploadfile)
        {
            Dictionary<string, object> restulToReturn = new Dictionary<string, object>();
            if (uploadfile != null && uploadfile.ContentLength > 0)
            {
                //ExcelDataReader works on binary excel file
                Stream stream = uploadfile.InputStream;
                //We need to written the Interface.
                IExcelDataReader reader = null;
                if (uploadfile.FileName.EndsWith(".xls"))
                {
                    //reads the excel file with .xls extension
                    reader = ExcelReaderFactory.CreateBinaryReader(stream);
                }
                else if (uploadfile.FileName.EndsWith(".xlsx"))
                {
                    //reads excel file with .xlsx extension
                    reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                }
                else
                {
                    //Shows error if uploaded file is not Excel file
                    restulToReturn.Add(Message.ERROR_TYPE, Message.ERROR_FILE_NOT_TYPE_EXCEL);
                    return restulToReturn;
                }
                //treats the first row of excel file as Coluymn Names
                reader.IsFirstRowAsColumnNames = true;
                //Adding reader data to DataSet()
                DataSet result = reader.AsDataSet();
                if (result.Tables.Count > 0)
                {
                    DataTable table = result.Tables[0];
                    table.TableName = uploadfile.FileName;
                    reader.Close();
                    restulToReturn.Add("excel", table);
                    return restulToReturn;
                    //return table;
                }
                else
                {
                    restulToReturn.Add(Message.ERROR_TYPE, Message.ERRO_EXCEL_NOT_CONTAIN_NO_ROW_DATA);
                    return restulToReturn;
                }
            }
            
            restulToReturn.Add(Message.ERROR_TYPE, Message.ERROR_FILE_NOT_SENT);
            return restulToReturn;
        }
    }
}
