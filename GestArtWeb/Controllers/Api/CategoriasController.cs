﻿using GestArtWeb.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using GestArtWeb.Dtos;
using AutoMapper;
using System;

namespace GestArtWeb.Controllers.Api
{
    public class CategoriasController : ApiController
    {
        private GestArtWebData db;

        public CategoriasController()
        {
            db = new GestArtWebData();
        }

        // GET /api/categorias
        public IHttpActionResult GetCategorias(string query = null)
        {
            var categoriasQuery = db.mage_categoria.OrderBy(c => c.nombre);

            if (!String.IsNullOrWhiteSpace(query))
                categoriasQuery = categoriasQuery.Where(c => c.nombre.Contains(query)).OrderBy(c => c.nombre);

            var categoriaDtos = categoriasQuery.ToList().Select(Mapper.Map<mage_categoria, CategoriaDto>);
            return Ok(categoriaDtos);
        }

        // GET /api/categorias/1
        public IHttpActionResult GetCategoria(int id)
        {
            var categoria = db.mage_categoria.SingleOrDefault(c => c.entity_id == id);

            if (categoria == null)
                return NotFound();

            return Ok(Mapper.Map<mage_categoria, CategoriaDto>(categoria));
        }

        // POST /api/categorias
        [HttpPost]
        public IHttpActionResult CreateCategoria(CategoriaDto categoriaDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var categoria = Mapper.Map<CategoriaDto, mage_categoria>(categoriaDto);

            db.mage_categoria.Add(categoria);
            db.SaveChanges();

            categoriaDto.entity_id = categoria.entity_id;

            return Created(new Uri(Request.RequestUri+"/"+categoria.entity_id), categoriaDto);
        }


        // PUT /api/categorias/1
        [HttpPut]
        public void UpdateCategoria(int id, CategoriaDto categoriaDto)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var categoriaInDb = db.mage_categoria.SingleOrDefault(c => c.entity_id == id);

            if (categoriaInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            Mapper.Map(categoriaDto, categoriaInDb);

            db.SaveChanges();
        }

        //DELETE /api/categorias/1
        [HttpDelete]
        public void DeleteCategoria(int id)
        {
            var categoriaInDb = db.mage_categoria.SingleOrDefault(c => c.entity_id == id);

            if (categoriaInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            db.mage_categoria.Remove(categoriaInDb);
            db.SaveChanges();
        }

    }
}
