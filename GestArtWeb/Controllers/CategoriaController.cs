﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GestArtWeb.Models;
using GestArtWeb.Helper;
using MySql.Data.MySqlClient;

namespace GestArtWeb.Controllers
{
    public class CategoriaController : Controller
    {
        private GestArtWebData db = new GestArtWebData();
        private DbMySQLServer dbMysql = new DbMySQLServer();
        private const string QUERY_LAST_ENTITY_ID_ON_MAGENTO = "SELECT entity_id+1 FROM catalog_category_entity ORDER BY entity_id DESC LIMIT 1",
            //INSERT_HEAD_NEW_CATEGORY_ON_MAGENTO = "insert into catalog_category_entity values(@entityId, 3, 3, @parentId, now(), now(), (select* from (select concat(path,'/',@entityId) from catalog_category_entity where entity_id = @parentId) kk), (select* from (select position+1 from catalog_category_entity where parent_id = @parentId order by position desc limit 1) kk), @level, 0);",
            INSERT_OR_UPDATE_HEAD_CATEGORY_ON_MAGENTO = "insert into catalog_category_entity values(@entityId, 3,3, @parentId, now(), now(), (select* from (select concat(path,'/',@entityId) from catalog_category_entity where entity_id = @parentId) kk), (select* from (select count(*)+1 from catalog_category_entity where parent_id = @parentId) kk), @level, 0) ON DUPLICATE KEY UPDATE parent_id=@parentId, level=@level, path=(select* from (select concat(path,'/',@entityId) from catalog_category_entity where entity_id = @parentId) kk),position=(select* from (select position+1 from catalog_category_entity where parent_id = @parentId order by position desc limit 1) kk);",
            //INSERT_NAME_NEW_CATEGORY_ON_MAGENTO = "insert into catalog_category_entity_varchar values(null, 3, 41, 0, @entityId, @value);",
            INSERT_OR_UPDATE_NAME_CATEGORY_ON_MAGENTO = "INSERT INTO catalog_category_entity_varchar VALUES((select * from (select IF(count(*) > 0, value_id, null) from catalog_category_entity_varchar where entity_id = @entityId and attribute_id = 41) kk), 3, 41, 0, @entityId, @value) ON DUPLICATE KEY UPDATE value=@value;",
            //INSERT_ATTRIBUTE_INT_NEW_CATEGORY_ON_MAGENTO = "insert into catalog_category_entity_int values(null, 3, @attributeId, 0, @entityId, @value);";
            INSERT_OR_UPDATE_ATTRIBUTE_INT_CATEGORY_ON_MAGENTO = "insert into catalog_category_entity_int values((select * from (select if(count(*) > 0, value_id, null) from catalog_category_entity_varchar where entity_id = @entityId and attribute_id = @attributeId) kk), 3, @attributeId, 0, @entityId, @value) on duplicate key update value=@value;";
        private const int ATTRIBUTE_INCLUDE_IN_MENU = 67, ATTRIBUTE_IS_ACTIVE = 42, ATTRIBUTE_IS_ANCHOR = 51, ATTRIBUTE_NAME = 41;
        private int[] attributesInt = {ATTRIBUTE_INCLUDE_IN_MENU, ATTRIBUTE_IS_ACTIVE, ATTRIBUTE_IS_ANCHOR };

        // GET: Categoria
        public async Task<ActionResult> Index(int? page, string searchValue)
        {
            ViewData["SearchValue"] = !String.IsNullOrEmpty(searchValue) ? searchValue : "";
            var categories = db.mage_categoria.Where(c => c.entity_id != 1 && c.entity_id != 2).Include(m => m.mage_categoria2);

           if (!String.IsNullOrEmpty(searchValue))
            {
                categories = categories.Where(c => c.nombre.Contains(searchValue) || c.entity_id.ToString() == searchValue);
            }

            return PartialView(await Pagination<mage_categoria>.CreateAsync(categories.OrderBy(c => c.nivel).AsNoTracking(), page ?? 1, Pagination<mage_categoria>.PAGE_SIZE));
        }

        public async Task<ActionResult> ListFilter(string searchValue) {
            ViewData["SearchValue"] = !String.IsNullOrEmpty(searchValue) ? searchValue : "";
            var categories = db.mage_categoria.Where(c => c.entity_id != 1 && c.entity_id != 2).Include(m => m.mage_categoria2);

            if (!String.IsNullOrEmpty(searchValue))
            {
                categories = categories.Where(c => c.nombre.Contains(searchValue) || c.entity_id.ToString() == searchValue);
            }

            return PartialView("_ListFilter", await Pagination<mage_categoria>.CreateAsync(categories.OrderBy(c => c.entity_id).AsNoTracking(), 1, Pagination<mage_categoria>.PAGE_SIZE));
        }

        public FileContentResult ExportExcel()
        {
            List<mage_categoria> categories = db.mage_categoria.Where(c => c.entity_id != 1 && c.entity_id != 2).Include(m => m.mage_categoria2).OrderBy(c => c.nivel).ToList();

            string[,] columns = new string[,] { };

            byte[] filecontent = ExportToExcel.ExportExcel(ExportToExcel.ListCategoriesoDataTable(categories), "Categorías Web", columns);
            return File(filecontent, ExportToExcel.ExcelContentType, "categories.xlsx");
        }
        


        // GET: Categoria/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            mage_categoria mage_categoria = await db.mage_categoria.FindAsync(id);
            if (mage_categoria == null)
            {
                return HttpNotFound();
            }
            return View(mage_categoria);
        }

        // GET: Categoria/Details/5
        public async Task<ActionResult> SubCategorias(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            mage_categoria mage_categoria = await db.mage_categoria.FindAsync(id);
            if (mage_categoria == null)
            {
                return HttpNotFound();
            }
            return PartialView("_SubCategories",mage_categoria);
        }

        public async Task<ActionResult> ProductsOfCategory(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            mage_categoria mage_categoria = await db.mage_categoria.FindAsync(id);
            if (mage_categoria == null)
            {
                return HttpNotFound();
            }
            return PartialView("_ProductsOfCategory", mage_categoria);
        }

        // GET: Categoria/Create
        public ActionResult Create()
        {
            return PartialView();
        }

        private mage_categoria CreateNewCategory(mage_categoria parent, int newEntityAvailable, string name) {
            try
            {
                mage_categoria newCat = new mage_categoria();
                newCat.entity_id = newEntityAvailable;
                newCat.nombre = name;
                newCat.padre = parent.entity_id;
                newCat.es_activa = 1;
                newCat.es_anchor = 0;
                newCat.incluida_en_menu = 1;
                newCat.nivel = parent.nivel + 1;
                db.mage_categoria.Add(newCat);
                db.SaveChanges();
                return newCat;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private int? GetNewEntityIdForCategory()
        {
            int? newEntityAvailable;
            try
            {
                newEntityAvailable = Convert.ToInt32(dbMysql.ExecuteSimpleQuery(QUERY_LAST_ENTITY_ID_ON_MAGENTO));
            }
            catch (Exception ex)
            {
                newEntityAvailable = null;
            }
            finally
            {
                dbMysql.Dispose();
            }
            return newEntityAvailable;
        }

        private bool? InsertOrUpdateçCategoryOnMagento(mage_categoria newCat)
        {
            List<QueryTransaction> queries = new List<QueryTransaction>();
            List<Parameter> Parameters = new List<Parameter>();

            Parameters.Add(new Parameter("@entityId", MySqlDbType.Int32, 5, 5, 0, newCat.entity_id));
            Parameters.Add(new Parameter("@parentId", MySqlDbType.Int32, 5, 5, 0, newCat.padre));
            Parameters.Add(new Parameter("@level", MySqlDbType.Int32, 5, 5, 0, newCat.nivel));
            queries.Add(new QueryTransaction(INSERT_OR_UPDATE_HEAD_CATEGORY_ON_MAGENTO, Parameter.CreateParametersMysql(Parameters)));
            Parameters.Clear();

            Parameters.Add(new Parameter("@entityId", MySqlDbType.Int32, 5, 5, 0, newCat.entity_id));
            Parameters.Add(new Parameter("@value", MySqlDbType.VarChar, 100, 5, 0, newCat.nombre));
            queries.Add(new QueryTransaction(INSERT_OR_UPDATE_NAME_CATEGORY_ON_MAGENTO, Parameter.CreateParametersMysql(Parameters)));
            Parameters.Clear();

            foreach (int attributeId in attributesInt)
            {
                Parameters.Add(new Parameter("@entityId", MySqlDbType.Int32, 5, 5, 0, newCat.entity_id));
                Parameters.Add(new Parameter("@attributeId", MySqlDbType.Int32, 5, 5, 0, attributeId));
                Parameters.Add(new Parameter("@value", MySqlDbType.Int32, 5, 5, 0, attributeId == ATTRIBUTE_INCLUDE_IN_MENU ? newCat.incluida_en_menu : attributeId == ATTRIBUTE_IS_ACTIVE ? newCat.es_activa : newCat.es_anchor));
                queries.Add(new QueryTransaction(INSERT_OR_UPDATE_ATTRIBUTE_INT_CATEGORY_ON_MAGENTO, Parameter.CreateParametersMysql(Parameters)));
                Parameters.Clear();
            }

            try
            {
                return dbMysql.ExecuteTransaction(queries);
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        private bool DeteleCategory(mage_categoria cat)
        {
            try
            {
                db.mage_categoria.Remove(cat);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        // POST: Categoria/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<JsonResult> Create(string dataNewCategory)
        {
            var result = new Dictionary<string,string>();
            var dataCategory = dataNewCategory.Split('&');
            const string KEY_TYPE = "Type";
            const string KEY_MESSAGES = "Messages";
            const string VALUE_TYPE_ERROR = "Error";
            const string VALUE_TYPE_SUCCESS = "Success";

            if (dataCategory.Length == 2)
            {
                string name = dataCategory[0].Split('=')[1];
                int parentId = 0;
                int? newEntityAvailable = GetNewEntityIdForCategory();

                try
                {
                    parentId = Convert.ToInt32(dataCategory[1].Split('=')[1]);
                }
                catch (Exception ex)
                {
                    result.Add(KEY_TYPE, VALUE_TYPE_ERROR);
                    result.Add(KEY_MESSAGES, Message.ERROR_CATEGORY_CODE_PARENT);
                    return Json(result);
                }

                if (!newEntityAvailable.HasValue)
                {
                    result.Add(KEY_TYPE, VALUE_TYPE_ERROR);
                    result.Add(KEY_MESSAGES, Message.ERROR_CATEGORY_TRY_GET_NEW_ENTITY_ID);
                    return Json(result);
                }
                int tmpEntityId = Convert.ToInt32(newEntityAvailable);
                var newCat = 
                    db.mage_categoria.FirstOrDefault(c => c.nombre == name && c.padre == parentId) ?? 
                    db.mage_categoria.FirstOrDefault(c => c.entity_id == tmpEntityId);

                if (newCat == null)
                {
                    mage_categoria parent = await db.mage_categoria.FirstOrDefaultAsync(c => c.entity_id == parentId);

                    if (parent != null)
                    {
                        if (parent.nivel >= 2 && parent.nivel <= 3)
                        {
                            newCat = CreateNewCategory(parent,Convert.ToInt32(newEntityAvailable), name);
                            if (newCat != null)
                            {
                                bool? catInsertedOnMagento = InsertOrUpdateçCategoryOnMagento(newCat);
                                if (catInsertedOnMagento.HasValue)
                                {
                                    if (Convert.ToBoolean(catInsertedOnMagento))
                                    {
                                        result.Add(KEY_TYPE, VALUE_TYPE_SUCCESS);
                                        result.Add(KEY_MESSAGES, String.Format(Message.SUCCESS_CATEGORY_ADDED, newCat.entity_id, newCat.nombre, newCat.padre, parent.nombre));
                                    }
                                    else
                                    {
                                        result.Add(KEY_TYPE, VALUE_TYPE_ERROR);
                                        result.Add(KEY_MESSAGES, Message.ERROR_CATEGORY_NOT_INSERT_ON_MAGENTO);
                                        if (!DeteleCategory(newCat))
                                        {
                                            result[KEY_MESSAGES] += String.Format(Message.ERROR_CATEGORY_CREATE_IN_ALFA_BUT_NOT_UPLOAD_ON_MAGENTO, newCat.entity_id);
                                        }
                                        
                                    }
                                }
                                else
                                {
                                    result.Add(KEY_TYPE, VALUE_TYPE_ERROR);
                                    result.Add(KEY_MESSAGES, Message.ERROR_CATEGORY_ROLLBACK_CREATE_ON_MAGENTO);
                                    if (!DeteleCategory(newCat))
                                    {
                                        result[KEY_MESSAGES] += String.Format(Message.ERROR_CATEGORY_CREATE_IN_ALFA_BUT_NOT_UPLOAD_ON_MAGENTO, newCat.entity_id);
                                    }
                                }
                            }
                            else
                            {
                                result.Add(KEY_TYPE, VALUE_TYPE_ERROR);
                                result.Add(KEY_MESSAGES, Message.ERROR_CATEGORY_NOT_CREATED_IN_ALFA);
                            }
                        }
                        else
                        {
                            result.Add(KEY_TYPE, VALUE_TYPE_ERROR);
                            result.Add(KEY_MESSAGES, Message.ERROR_CATEGORY_PARENT_WITH_LEVEL_THREE);
                        }
                    }
                    else
                    {
                        result.Add(KEY_TYPE, VALUE_TYPE_ERROR);
                        result.Add(KEY_MESSAGES, Message.ERROR_CATEGORY_PARENT_NOT_EXISTS);
                    }

                }
                else
                {
                    result.Add(KEY_TYPE, VALUE_TYPE_ERROR);
                    result.Add(KEY_MESSAGES, String.Format(Message.ERROR_CATEGORY_ALREADY_EXISTS, newCat.nombre, newCat.padre, newCat.entity_id));
                }
            }
            else
            {
                result.Add(KEY_TYPE, VALUE_TYPE_ERROR);
                result.Add(KEY_MESSAGES, Message.ERROR_CATEGORY_ALL_DATA_REQUIRED);
            }

            return Json(result);
        }

        // GET: Categoria/Edit/5
        public async Task<JsonResult> Edit(int? id)
        {
            Dictionary<string, string> dataCat = new Dictionary<string, string>();
            mage_categoria catFound = await db.mage_categoria.FirstOrDefaultAsync(c => c.entity_id > 2 && c.entity_id == id);
            if (catFound == null)
            {
                dataCat.Add("Exists", "0");
                dataCat.Add("Messages", String.Format(Message.ERROR_CATEGORY_ID_NOT_EXISTS, id));
            }
            else
            {
                dataCat.Add("Exists", "1");
                dataCat.Add("EntityId", catFound.entity_id.ToString());
                dataCat.Add("Name", catFound.nombre);
                dataCat.Add("Parent", catFound.padre.ToString());
                dataCat.Add("Active", catFound.es_activa.ToString());
            }
            return Json(dataCat, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetFormEdit()
        {
            return PartialView("Edit");
        }

        private bool UpdateCategory(mage_categoria catFound, string name, int parentId, int isActive)
        {
            bool catUpdated = false;
            bool thereIsSomeChange = false;
            if (catFound.nombre != name)
            {
                catFound.nombre = name;
                if (!thereIsSomeChange)
                {
                    thereIsSomeChange = true;
                }
            }
            if (catFound.padre != parentId)
            {
                catFound.padre = parentId;
                if (!thereIsSomeChange)
                {
                    thereIsSomeChange = true;
                }
            }
            if (catFound.es_activa != isActive)
            {
                catFound.es_activa = isActive;
                if (!thereIsSomeChange)
                {
                    thereIsSomeChange = true;
                }
            }
            if (thereIsSomeChange)
            {
                db.Entry(catFound).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    catUpdated = true;
                }
                catch (Exception ex)
                {
                    catUpdated = false;
                }
            }
            return catUpdated;
        }

        // POST: Categoria/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<JsonResult> Edit(string dataUpdateCategory)
        {
            var result = new Dictionary<string, string>();
            const string KEY_TYPE = "Type";
            const string KEY_MESSAGES = "Messages";
            const string VALUE_TYPE_ERROR = "Error";
            const string VALUE_TYPE_SUCCESS = "Success";
            var dataCategory = dataUpdateCategory.Split('&');
            int entityId = Convert.ToInt32(dataCategory[0].Split('=')[1]);
            string name = dataCategory[1].Split('=')[1];
            int parentId = Convert.ToInt32(dataCategory[2].Split('=')[1]);
            int isActive = dataCategory.Length == 4 ? Convert.ToInt32(dataCategory[3].Split('=')[1]) : 0;
            var catFound = await db.mage_categoria.FirstOrDefaultAsync(c => c.entity_id == entityId);

            if (catFound != null)
            {
                mage_categoria parent = await db.mage_categoria.FirstOrDefaultAsync(c => c.entity_id == parentId);
                if (parent != null)
                {
                    if (parent.nivel >= 2 && parent.nivel <= 3)
                    {
                        if (UpdateCategory(catFound, name, parentId, isActive))
                        {
                            bool? catInsertedOnMagento = InsertOrUpdateçCategoryOnMagento(catFound);
                            if (catInsertedOnMagento.HasValue)
                            {
                                if (Convert.ToBoolean(catInsertedOnMagento))
                                {
                                    result.Add(KEY_TYPE, VALUE_TYPE_SUCCESS);
                                    result.Add(KEY_MESSAGES, String.Format(Message.SUCCESS_CATEGORY_ADDED, catFound.entity_id, catFound.nombre, catFound.padre, catFound.nombre));
                                }
                                else
                                {
                                    result.Add(KEY_TYPE, VALUE_TYPE_ERROR);
                                    result.Add(KEY_MESSAGES, Message.ERROR_CATEGORY_NOT_INSERT_ON_MAGENTO);

                                    result[KEY_MESSAGES] += String.Format(Message.ERROR_CATEGORY_CREATE_IN_ALFA_BUT_NOT_UPLOAD_ON_MAGENTO, catFound.entity_id);

                                }
                            }
                            else
                            {
                                result.Add(KEY_TYPE, VALUE_TYPE_ERROR);
                                result.Add(KEY_MESSAGES, Message.ERROR_CATEGORY_ROLLBACK_CREATE_ON_MAGENTO);

                                result[KEY_MESSAGES] += String.Format(Message.ERROR_CATEGORY_CREATE_IN_ALFA_BUT_NOT_UPLOAD_ON_MAGENTO, catFound.entity_id);

                            }
                        }
                        else
                        {
                            result.Add(KEY_TYPE, VALUE_TYPE_ERROR);
                            result.Add(KEY_MESSAGES, Message.ERROR_CATEGORY_UPDATE_IN_ALFA);
                        }
                    }
                    else
                    {
                        result.Add(KEY_TYPE, VALUE_TYPE_ERROR);
                        result.Add(KEY_MESSAGES, Message.ERROR_CATEGORY_PARENT_WITH_LEVEL_THREE);
                    }
                }
                else
                {
                    result.Add(KEY_TYPE, VALUE_TYPE_ERROR);
                    result.Add(KEY_MESSAGES, Message.ERROR_CATEGORY_PARENT_NOT_EXISTS);
                }
            }
            else
            {
                result.Add(KEY_TYPE, VALUE_TYPE_ERROR);
                result.Add(KEY_MESSAGES, String.Format(Message.ERROR_CATEGORY_ID_NOT_EXISTS, entityId));
            }

            return Json(result);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
