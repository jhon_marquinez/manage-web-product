﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GestArtWeb.Models;
using System.IO;
using Excel;
using System.Configuration;
using MySql.Data.MySqlClient;
using GestArtWeb.Helper;
using Renci.SshNet;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Web.Services;
using GestArtWeb.MagentoServices;
using System.Data.Entity.Infrastructure;

namespace GestArtWeb.Controllers
{
    public class ProductoController : Controller
    {
        private GestArtWebData db = new GestArtWebData();
        private DbMySQLServer dbMysql = null;
        private const string QUERY_INSERT_DATA_ATTRIBUTE_NEW_OR_UPDATE_MAGENTO = "INSERT INTO ad_new_update_product (sku,value,attributo_id,date_register,file_name) VALUES(@sku,@value,@atributo,@date,@fileName) ON DUPLICATE KEY UPDATE value=@value;";
        private const string QUERY_CALL_PROCEDURE_NEW_UPDATE_PRODUCT = "CALL ad_add_new_update_product(@p_date,@p_sku,@p_file_name);";
        private const string QUERY_DELETE_CATEGORY_IF_EXIST_FOR_PRODUCT = "DELETE FROM ad_new_update_product WHERE sku = @sku AND value = @value AND file_name = @fileName AND date_register = @date;";
        private const string QUERY_LAST_LOG_FOR_FILE = "SELECT status, message, date_error, file_name FROM ad_log_new_update_product WHERE IF(@p_date IS NOT NULL, date_error = @p_date, DATE(date_error)=DATE(NOW())) AND file_name = @p_file_name ORDER BY date_error DESC LIMIT 1;";
        private const string QUERY_GET_ENTITY_FOR_PRODUCT = "SELECT entity_id FROM catalog_product_entity WHERE sku=@sku;";
        private const int UPDATE_PROMOTIONS_YES = 1;
        private const int MANUFACTURE = 81;
        private const int PRICE = 75;
        private const int UNIDAD_ENVACE = 823;
        private const int STATUS = 96;
        private const int IS_MULTIPLE = 824;
        private const int EAN = 144;
        private const int PROMOTIONS = 139;
        private const int ORDEN = 836;
        private const int VISIBILITY = 102;
        private const int NAME = 71;
        private int[] COMMON_ATTRIBUTES = new int[] { MANUFACTURE, PRICE, UNIDAD_ENVACE, STATUS, IS_MULTIPLE, EAN, PROMOTIONS, ORDEN, VISIBILITY, NAME };
        private int TOTAL_ROW_IN_TABLE_COPY_DATA_PRODUCT = 20;

        [HttpPost]
        public JsonResult RefreshcumentProduct(string sku, string nameDoc) {
            MagentoServices.PortTypeClient clientMagentoWsdl = new PortTypeClient();
            var sesionMagentoWsdl = clientMagentoWsdl.login("alfadyser", "4lf4dys3r");

            var result = clientMagentoWsdl.call(
                sesionMagentoWsdl,
                "productos.producto",
                string.Format("<magento><producto><sku>{0}</sku><ficha_tecnica>{1}.pdf</ficha_tecnica></producto></magento>",sku,nameDoc)
                );

            clientMagentoWsdl.endSession(sesionMagentoWsdl);
            sesionMagentoWsdl = string.Empty;
            clientMagentoWsdl = null;

            return result.ToString().Contains("ERROR:") ? Json(0) : Json(1);
        }
        // GET: Producto
        public async Task<ActionResult> Index(bool ajaxQuery = false, string skuOrName = "")
        {
            IQueryable<mage_articulo> mage_articulo;
            if (skuOrName != String.Empty && skuOrName.Length > 0)
            {
                mage_articulo = db.mage_articulo.Where(p => p.sku.Length == 5 && (p.sku.Contains(skuOrName) || p.mage_articulo_atributo_valor.FirstOrDefault(a => a.id_atributo == 71).value_varchar.Contains(skuOrName)));
            }
            else
            {
                mage_articulo = db.mage_articulo.Where(m => m.sku.Length == 5).Include(m => m.mage_familia).Take(10);
            }
            if (ajaxQuery)
            {
                return PartialView(await mage_articulo.ToListAsync());
            }
            else
            {
                return View(await mage_articulo.ToListAsync());
            }
        }

        // GET: Producto/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            mage_articulo mage_articulo = await db.mage_articulo.FindAsync(id);
            if (mage_articulo == null)
            {
                return HttpNotFound();
            }
            return View(mage_articulo);
        }

        private bool IsItSomeCategoryColumn(string nameColumn)
        {
            return (nameColumn == "CAT1" || nameColumn == "CAT2" || nameColumn == "CAT3");
        }

        private string GetMessage(string message, List<string> args)
        {
            return String.Format(message, args.ToArray());
        }

        public DataTable LoadExcelInDataTable(HttpPostedFileBase uploadfile)
        {
            ViewData["messageError"] = null;
            if (uploadfile != null && uploadfile.ContentLength > 0)
            {
                //ExcelDataReader works on binary excel file
                Stream stream = uploadfile.InputStream;
                //We need to written the Interface.
                IExcelDataReader reader = null;
                if (uploadfile.FileName.EndsWith(".xls"))
                {
                    //reads the excel file with .xls extension
                    reader = ExcelReaderFactory.CreateBinaryReader(stream);
                }
                else if (uploadfile.FileName.EndsWith(".xlsx"))
                {
                    //reads excel file with .xlsx extension
                    reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                }
                else
                {
                    //Shows error if uploaded file is not Excel file
                    ViewData["messageError"] = Message.ERROR_FILE_NOT_TYPE_EXCEL;
                    return null;
                }
                //treats the first row of excel file as Coluymn Names
                reader.IsFirstRowAsColumnNames = true;
                //Adding reader data to DataSet()
                DataSet result = reader.AsDataSet();
                if (result.Tables.Count > 0)
                {
                    DataTable table = result.Tables[0];
                    table.TableName = uploadfile.FileName;
                    reader.Close();
                    return table;
                }
                else {
                    ViewData["messageError"] = Message.ERRO_EXCEL_NOT_CONTAIN_NO_ROW_DATA;
                    return null;
                }
            }else ViewData["messageError"] = Message.ERROR_FILE_NOT_SENT;

            return null;
        }

        private Task<bool> UploadProductosAttributesInTemporaryTableOnMagentoAsync(List<mage_articulo> articles, string fileName = null)
        {
            return Task.Run(() => UploadProductosAttributesInTemporaryTableOnMagento(articles, fileName));
        }
        private bool UploadProductosAttributesInTemporaryTableOnMagento(mage_articulo article, string fileName = null)
        {
            return UploadProductosAttributesInTemporaryTableOnMagento(new List<mage_articulo> { article }, fileName);
        }
        private bool UploadProductosAttributesInTemporaryTableOnMagento(List<mage_articulo> articles, string fileName = null)
        {
            bool someProductWasUploadCorrectly = false;
            var dbMysql = new DbMySQLServer();
            string date = DateTime.Today.ToString("yyyyMMdd");
            List<Parameter> parameters = new List<Parameter>();
            parameters.Add(new Parameter("@fileName", MySqlDbType.VarChar, 65532, 0, 0, fileName ?? String.Format("Subido sin Excel", date)));

            foreach (mage_articulo article in articles)
            {
                bool thereWasAnError = false;
                parameters.Add(new Parameter("@sku", MySqlDbType.VarChar, 5, 5, 0, article.sku));
                parameters.Add(new Parameter("@date", MySqlDbType.VarChar, 5, 5, 0, date));

                foreach (mage_articulo_atributo_valor attr in article.mage_articulo_atributo_valor)
                {
                    try
                    {
                        parameters.Add(new Parameter("@atributo", MySqlDbType.Int32, 5, 5, 0, attr.id_atributo));
                        var paramValue = new Parameter("@value", MySqlDbType.VarChar, 65532, 0, 0,
                                        attr.mage_atributo.tipo == "int"
                                            ? attr.value_int != null
                                                ? attr.mage_atributo.filtrable == 1 || attr.mage_atributo.id_atributo == 92
                                                    ? attr.mage_atributo.mage_atributo_opcion.FirstOrDefault(o => o.id_opcion == attr.value_int) != null
                                                        ? attr.mage_atributo.mage_atributo_opcion.FirstOrDefault(o => o.id_opcion == attr.value_int).valor
                                                        : ""
                                                    : attr.value_int.ToString()
                                                : ""
                                            : attr.mage_atributo.tipo == "decimal" ? attr.value_decimal != null ? attr.value_decimal.ToString().Replace(",",".") : ""
                                            : attr.mage_atributo.tipo == "varchar" ? attr.value_varchar != null ? attr.value_varchar : ""
                                            : attr.value_text != null ? attr.value_text : "");
                        parameters.Add(paramValue);

                        dbMysql.ExecuteSentenceCommand(QUERY_INSERT_DATA_ATTRIBUTE_NEW_OR_UPDATE_MAGENTO, Parameter.CreateParametersMysql(parameters));

                        parameters.RemoveRange(3, 2);
                    }
                    catch (Exception ex)
                    {
                        AddMessagesForView(Message.ERROR_TYPE, new List<Message>() {
                            new Message(
                                        article.sku,
                                        GetMessage(Message.ERROR_UPLOAD_ATTRIBUTES_OF_PRODUCT, new List<string> { article.sku, attr.mage_atributo.nombre }),
                                        Message.GetBtnAction(article.sku, "/Producto/...", "Reintentar")
                                        )
                        });
                        thereWasAnError = true;
                        break;
                    }
                    finally
                    {
                        dbMysql.Dispose();
                    }
                }
                parameters.RemoveRange(1, 2);
                if (!thereWasAnError && !someProductWasUploadCorrectly)
                {
                    someProductWasUploadCorrectly = true;
                }
            }
            dbMysql = null;
            return someProductWasUploadCorrectly;
        }

        private Task UploadProductoFamilyInTemporaryTableOnMagentoAsync(List<mage_articulo> articles, string fileName = null)
        {
            return Task.Run(() => UploadProductoFamilyInTemporaryTableOnMagento(articles, fileName));
        }
        private void UploadProductoFamilyInTemporaryTableOnMagento(mage_articulo article, string fileName = null)
        {
            UploadProductoFamilyInTemporaryTableOnMagento(new List<mage_articulo> { article }, fileName);
        }
        private void UploadProductoFamilyInTemporaryTableOnMagento(List<mage_articulo> articles, string fileName = null)
        {
            var dbMysql = new DbMySQLServer();
            string date = DateTime.Today.ToString("yyyyMMdd");
            List<Parameter> parameters = new List<Parameter>();
            parameters.Add(new Parameter("@date", MySqlDbType.VarChar, 5, 5, 0, date));
            parameters.Add(new Parameter("@value", MySqlDbType.VarChar, 65532, 0, 0, "Familia ID"));
            parameters.Add(new Parameter("@fileName", MySqlDbType.VarChar, 65532, 0, 0, fileName ?? String.Format("Subido sin Excel", date)));
            

            foreach (mage_articulo article in articles)
            {
                parameters.Add(new Parameter("@sku", MySqlDbType.VarChar, 5, 5, 0, article.sku));
                parameters.Add(new Parameter("@atributo", MySqlDbType.Int32, 5, 5, 0, article.id_familia));

                try
                {
                    dbMysql.ExecuteSentenceCommand(QUERY_INSERT_DATA_ATTRIBUTE_NEW_OR_UPDATE_MAGENTO, Parameter.CreateParametersMysql(parameters));
                }
                catch (Exception ex)
                {
                    AddMessagesForView(Message.ERROR_TYPE, new List<Message>() {
                            new Message(
                                        article.sku,
                                        GetMessage(Message.ERROR_UPLOAD_FAMILY_OF_PRODUCT, new List<string> { article.sku, article.id_familia.ToString(), article.mage_familia.nombre }),
                                        Message.GetBtnAction(article.sku, "/Producto/...", "Reintentar")
                                        )
                        });
                    break;
                }
                finally
                {
                    dbMysql.Dispose();
                }

                parameters.RemoveRange(3,2);
            }
            dbMysql = null;

        }


        private bool DeleteCategoryForProductToCreateOrUpdate(List<Parameter> parameters, mage_articulo article, mage_categoria cat)
        {
            var dbMysql = new DbMySQLServer();
            bool categoryDeleted = true;
            try
            {
                dbMysql.ExecuteSentenceCommand(QUERY_DELETE_CATEGORY_IF_EXIST_FOR_PRODUCT, Parameter.CreateParametersMysql(parameters));
            }
            catch (Exception ex)
            {
                AddMessagesForView(Message.ERROR_TYPE, new List<Message>() {
                            new Message(
                                        article.sku,
                                        GetMessage(Message.ERROR_UPLOAD_CATEGORIES_OF_PRODUCT, new List<string> { article.sku, cat.entity_id.ToString(), cat.nombre }),
                                        Message.GetBtnAction(article.sku, "/Producto/...", "Reintentar")
                                        )
                        });
                categoryDeleted = false;
            }
            finally
            {
                dbMysql.Dispose();
            }
            dbMysql = null;
            return categoryDeleted;
        }

        private Task UploadProductCategoriesInTemporaryTableOnMagentoAsync(List<mage_articulo> articles, string fileName = null)
        {
            return Task.Run(() => UploadProductCategoriesInTemporaryTableOnMagento(articles, fileName));
        }
        private void UploadProductCategoriesInTemporaryTableOnMagento(mage_articulo article, string fileName = null)
        {
            UploadProductCategoriesInTemporaryTableOnMagento(article, fileName);
        }
        private void UploadProductCategoriesInTemporaryTableOnMagento(List<mage_articulo> articles, string fileName = null)
        {
            var dbMysql = new DbMySQLServer();
            string date = DateTime.Today.ToString("yyyyMMdd");
            List<Parameter> parameters = new List<Parameter>();
            parameters.Add(new Parameter("@date", MySqlDbType.VarChar, 5, 5, 0, date));
            parameters.Add(new Parameter("@fileName", MySqlDbType.VarChar, 65532, 0, 0, fileName ?? String.Format("Subido sin Excel", date)));

            foreach (mage_articulo article in articles)
            {
                parameters.Add(new Parameter("@sku", MySqlDbType.VarChar, 5, 5, 0, article.sku));

                foreach (mage_categoria cat in article.mage_categoria)
                {
                    parameters.Add(new Parameter("@atributo", MySqlDbType.Int32, 5, 5, 0, cat.entity_id));
                    switch (cat.nivel)
                    {
                        case 2:
                            parameters.Add(new Parameter("@value", MySqlDbType.VarChar, 65532, 0, 0, "CAT1"));
                            break;
                        case 3:
                            parameters.Add(new Parameter("@value", MySqlDbType.VarChar, 65532, 0, 0, "CAT2"));
                            break;
                        case 4:
                            parameters.Add(new Parameter("@value", MySqlDbType.VarChar, 65532, 0, 0, "CAT3"));
                            break;
                    }
                    try
                    {
                        if (DeleteCategoryForProductToCreateOrUpdate(parameters, article, cat))
                        {
                            dbMysql.ExecuteSentenceCommand(QUERY_INSERT_DATA_ATTRIBUTE_NEW_OR_UPDATE_MAGENTO, Parameter.CreateParametersMysql(parameters));
                        }
                    }
                    catch (Exception ex)
                    {
                        AddMessagesForView(Message.ERROR_TYPE, new List<Message>() {
                            new Message(
                                        article.sku,
                                        GetMessage(Message.ERROR_UPLOAD_CATEGORIES_OF_PRODUCT, new List<string> { article.sku, cat.entity_id.ToString(), cat.nombre }),
                                        Message.GetBtnAction(article.sku, "/Producto/...", "Reintentar")
                                        )
                        });
                        break;
                    }
                    finally
                    {
                        dbMysql.Dispose();
                    }
                    parameters.RemoveRange(3, 2);
                }
            }
            dbMysql = null;
        }

        private Task<bool> UploadDataProductsInTemporaryTableOnMagentoAsync(mage_articulo article, string fileName = null)
        {
            /*try
            {
                var articles = new List<mage_articulo> { article };
                var taskUploadAttribute = UploadProductosAttributesInTemporaryTableOnMagentoAsync(articles, fileName);
                var taskUploadCategories = UploadProductCategoriesInTemporaryTableOnMagentoAsync(articles, fileName);
                var taskUploadFamily = UploadProductoFamilyInTemporaryTableOnMagentoAsync(articles, fileName);

                await taskUploadAttribute;
                await taskUploadCategories;
                await taskUploadFamily;
            }
            catch (Exception ex)
            {
                ViewData[Message.ERROR_TYPE] = String.Format(Message.ERRO_UPLOAD_DATA_PRODUCTS_IN_TEMPORARY_TABLE_ON_MAGENTO);
            }
            return true;*/
            return UploadDataProductsInTemporaryTableOnMagentoAsync(new List<mage_articulo> { article }, fileName);
        }

        private async Task<bool> UploadDataProductsInTemporaryTableOnMagentoAsync(List<mage_articulo> articles, string fileName = null)
        {
            bool someProductWasUploadCorrectly = false;
            try
            {
                var taskUploadAttribute = UploadProductosAttributesInTemporaryTableOnMagentoAsync(articles, fileName);
                var taskUploadCategories = UploadProductCategoriesInTemporaryTableOnMagentoAsync(articles, fileName);
                var taskUploadFamily = UploadProductoFamilyInTemporaryTableOnMagentoAsync(articles, fileName);

                someProductWasUploadCorrectly = await taskUploadAttribute;
                await taskUploadCategories;
                await taskUploadFamily;
            }
            catch (Exception ex)
            {
                ViewData[Message.ERROR_TYPE] = String.Format(Message.ERRO_UPLOAD_DATA_PRODUCTS_IN_TEMPORARY_TABLE_ON_MAGENTO);
            }
            return someProductWasUploadCorrectly;
        }

        private void LocateAllAttributesInSpecicTablesForNewProducts(string fileName, mage_articulo article, string date = null)
        {
            LocateAllAttributesInSpecicTablesForNewProducts(fileName, article != null ? new List<mage_articulo> { article } : null, date);
        }
        private void LocateAllAttributesInSpecicTablesForNewProducts(string fileName, List<mage_articulo> articles = null, string date = null)
        {
            ViewData["messageSuccess"] = null;
            ViewData["messageError"] = null;
            var dbMysql = new DbMySQLServer();
            List<Parameter> parameters = new List<Parameter>();
            parameters.Add(new Parameter("@p_file_name", MySqlDbType.VarChar, 65532, 0, 0, fileName));
            if (date != null)
            {
                parameters.Add(new Parameter("@p_date", MySqlDbType.VarChar, 65532, 0, 0, date));
            }
            else
            {
                parameters.Add(new Parameter("@p_date", MySqlDbType.VarChar, 65532, 0, 0, DBNull.Value));
            }

            if (articles != null && articles.Count > 0)
            {
                foreach (mage_articulo article in articles)
                {
                    parameters.Add(new Parameter("@p_sku", MySqlDbType.VarChar, 65532, 0, 0, article.sku));
                    var param = Parameter.CreateParametersMysql(parameters);
                    try
                    {
                        dbMysql.ExecuteSentenceCommand(QUERY_CALL_PROCEDURE_NEW_UPDATE_PRODUCT, param);
                        var reader = dbMysql.ExecuteQuery(QUERY_LAST_LOG_FOR_FILE, param);
                        if (reader.HasRows)
                        {
                            reader.Read();
                            if (reader.GetString(0) == "SUCCESS")
                            {
                                AddMessagesForView(Message.SUCCESS_TYPE, new List<Message>() {
                                    new Message(
                                                article.sku,
                                                Message.SUCCESS_PRODUCT_ADDED
                                                )
                                });
                            }
                            else if (reader.GetString(0) == "ERROR")
                            {
                                AddMessagesForView(Message.ERROR_TYPE, new List<Message>() {
                                    new Message(
                                                article.sku,
                                                Message.ERROR_PRODUCT_NOT_ADDED,
                                                Message.GetBtnAction(article.sku, "/Producto/...", "Reintentar")
                                                )
                                });
                            }
                        }
                        parameters.RemoveAt(parameters.Count - 1);
                    }
                    catch (Exception ex)
                    {
                        AddMessagesForView(Message.ERROR_TYPE, new List<Message>() {
                            new Message(
                                        article.sku,
                                        Message.ERROR_CALL_PROCEDURE_NEW_UPDATE_PRODUCT,
                                        Message.GetBtnAction(article.sku, "/Producto/...", "Reintentar")
                                        )
                            });
                    }
                }
                    
            }
            else
            {
                parameters.Add(new Parameter("@p_sku", MySqlDbType.VarChar, 65532, 0, 0, DBNull.Value));
                var param = Parameter.CreateParametersMysql(parameters);
                try
                {
                    dbMysql.ExecuteSentenceCommand(QUERY_CALL_PROCEDURE_NEW_UPDATE_PRODUCT, param);
                    var reader = dbMysql.ExecuteQuery(QUERY_LAST_LOG_FOR_FILE, param);
                    if (reader.HasRows)
                    {
                        reader.Read();

                        if (reader.GetString(0) == "SUCCESS")
                        {
                            AddMessagesForView(Message.SUCCESS_TYPE, new List<Message>() {
                                    new Message(
                                                String.Empty,
                                                Message.SUCCESS_PRODUCT_ADDED
                                                )
                                });
                        }
                        else
                        {
                            ViewData[Message.SUCCESS_TYPE] = Message.ERROR_PRODUCT_NOT_ADDED;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ViewData[Message.ERROR_TYPE] = Message.ERROR_CALL_PROCEDURE_NEW_UPDATE_PRODUCT;
                }
                 // reindexar y limpiar cache
            }
        }


        private List<mage_articulo> GetListProductsOrEmptyProductsIfNotExitsSavingChange(string[] skus)
        {
            List<mage_articulo> articles = new List<mage_articulo>();
            foreach (string sku in skus)
            {
                var article = db.mage_articulo.FirstOrDefault(a => a.sku == sku);
                if (article == null && db.g733_articulos.FirstOrDefault(a => a.xarticulo_id == sku) != null)
                {
                    article = new mage_articulo();
                    article.sku = sku;
                    db.mage_articulo.Add(article);
                }
                if (article != null)
                {
                    articles.Add(article);
                }
            }
            db.SaveChanges();

            return articles;
        }

        private mage_articulo GetProductOrEmptyProductIfNotExitsSavingChange(string sku)
        {
            var article = db.mage_articulo.FirstOrDefault(a => a.sku == sku);
            if (article == null && db.g733_articulos.FirstOrDefault(a => a.xarticulo_id == sku) != null)
            {
                article = new mage_articulo();
                article.sku = sku;
                db.mage_articulo.Add(article);
                db.SaveChanges();
            }

            return article;
        }

        private Task UpdateCategoriesToProductWithoutSavingChangeAsync(mage_articulo article, string[] categories)
        {
            return Task.Run(() => UpdateCategoriesToProductWithoutSavingChange(article, categories));
        }
        private void UpdateCategoriesToProductWithoutSavingChange(mage_articulo article, string[] categories)
        {
            if (article.mage_categoria.Count > 3)
            {
                throw new Exception("El producto tiene más de una categoría y esto no esta permitido");
            }
            foreach (string category in categories)
            {
                var typeCat = category.Split('=')[0];
                var idCat = category.Split('=')[1];
                if (typeCat != null && idCat != null)
                {
                    int level = -1;
                    for (int a = 1; a <= categories.Length; a++)
                    {
                        if (typeCat == String.Format("cat{0}", a))
                        {
                            level = a+1;
                            break;
                        }
                    }

                    var newCatId = Convert.ToInt32(idCat);
                    var oldCat = article.mage_categoria.FirstOrDefault(c => c.nivel == level);
                    var newCat = db.mage_categoria.FirstOrDefault(c => c.nivel == level && c.entity_id == newCatId);
                    if (newCat != null && newCat.entity_id != oldCat.entity_id)
                    {
                        article.mage_categoria.Remove(article.mage_categoria.FirstOrDefault(c => c.nivel == level));
                        article.mage_categoria.Add(newCat);
                    }
                }
            }
            
        }

        private Task AddCategoriesToProductWithoutSavingChangeAsync(mage_articulo article, string[] categories)
        {
            return Task.Run(() => AddCategoriesToProductWithoutSavingChange(article, categories));
        }
        private void AddCategoriesToProductWithoutSavingChange(mage_articulo article, string[] categories)
        {
            if (categories != null) {
                foreach (string category in categories)
                {
                    var idCat = category.Split('=')[1];
                    if (idCat != null)
                    {
                        var entity_idCategory = Convert.ToInt32(idCat);
                        var cat = db.mage_categoria.FirstOrDefault(c => c.entity_id == entity_idCategory);
                        if (cat != null)
                        {
                            article.mage_categoria.Add(cat);
                        }
                    }
                }
            }
        }
        private Task UpdateAttributesToProductWithoutSavingChangeAsync(mage_articulo article, IEnumerable<string> attributes)
        {
            return Task.Run(() => UpdateAttributesToProductWithoutSavingChange(article, attributes));
        }
        private void UpdateAttributesToProductWithoutSavingChange(mage_articulo article, IEnumerable<string> attributes)
    {
            foreach (string attributeAndValue in attributes)
            {
                var attribute = Convert.ToInt16(attributeAndValue.Split('=')[0]);
                var attr = db.mage_atributo.FirstOrDefault(a => a.id_atributo == attribute);
                if (attr != null)
                {
                    var value = attributeAndValue.Split('=')[1];
                    var attrVal = article.mage_articulo_atributo_valor.FirstOrDefault(a => a.id_atributo == attribute);
                    if (attrVal == null)
                    {
                        attrVal = new mage_articulo_atributo_valor();
                        attrVal.sku = article.sku;
                        attrVal.id_atributo = attribute;
                        attrVal.mage_atributo = attr;
                        article.mage_articulo_atributo_valor.Add(attrVal);
                    }
                    if (!String.IsNullOrEmpty(value))
                    {
                        switch (attr.tipo)
                        {
                            case "int":
                                if (attrVal.value_int != Convert.ToInt32(value))
                                {
                                    attrVal.value_int = Convert.ToInt32(value);
                                }
                                break;
                            case "decimal":
                                if (attrVal.value_decimal != Convert.ToDecimal(value))
                                {
                                    attrVal.value_decimal = Convert.ToDecimal(value);
                                }
                                break;
                            case "varchar":
                                if (attrVal.value_varchar != value)
                                {
                                    attrVal.value_varchar = value;
                                }
                                break;
                            case "text":
                                if (attrVal.value_text != value)
                                {
                                    attrVal.value_text = value;
                                }
                                break;
                        }
                    }
                    else
                    {
                        switch (attr.tipo)
                        {
                            case "int":
                                attrVal.value_int = null;
                                break;
                            case "decimal":
                                attrVal.value_decimal = null;
                                break;
                            case "varchar":
                                attrVal.value_varchar = null;
                                break;
                            case "text":
                                attrVal.value_text = null;
                                break;
                        }
                    }
                }
            }
        }

        private Task AddAttributesToProductWithoutSavingChangeAsync(mage_articulo article, IEnumerable<string> attributes)
        {
            return Task.Run(() => AddAttributesToProductWithoutSavingChange(article, attributes));
        }
        private void AddAttributesToProductWithoutSavingChange(mage_articulo article, IEnumerable<string> attributes, bool checkoutIfSomeIntAttributeIsFiltrable = false)
        {
            foreach (string attributeAndValue in attributes)
            {
                if (attributeAndValue.Split('=')[0] == null)
                {
                    break;
                }
                var attribute = Convert.ToInt16(attributeAndValue.Split('=')[0]);
                var attr = db.mage_atributo.FirstOrDefault(a => a.id_atributo == attribute);
                mage_articulo_atributo_valor attrVal = null;
                if (attr != null)
                {
                    bool toUpdate = false, modified = false;
                    var value = attributeAndValue.Split('=')[1];
                    attrVal = article.mage_articulo_atributo_valor.FirstOrDefault(a => a.id_atributo == attribute);
                    if (attrVal != null)
                    {
                        toUpdate = true;
                    }
                    else {
                        attrVal = new mage_articulo_atributo_valor();
                    }
                    attrVal.sku = attrVal.sku ?? article.sku;
                    attrVal.id_atributo = attribute;
                    attrVal.mage_atributo = attrVal.mage_atributo ?? attr;
                    if (!String.IsNullOrEmpty(value))
                    {
                        switch (attr.tipo)
                        {
                            case "int":
                                if (checkoutIfSomeIntAttributeIsFiltrable && (attrVal.mage_atributo.id_atributo == 92 || attrVal.mage_atributo.filtrable == 1))
                                {
                                    var valueOption = db.mage_atributo_opcion.FirstOrDefault(o => o.valor == value && o.id_atributo == attrVal.id_atributo);
                                    if (valueOption != null)
                                    {
                                        if (attrVal.value_int != valueOption.id_opcion)
                                        {
                                            modified = true;
                                            attrVal.value_int = valueOption.id_opcion;
                                        }
                                        //attrVal.value_int = valueOption.id_opcion;
                                    }
                                    else
                                    {
                                        valueOption = new mage_atributo_opcion();
                                        valueOption.id_opcion = db.mage_atributo_opcion.OrderByDescending(o => o.id_opcion).First().id_opcion + 1;
                                        valueOption.id_atributo = Convert.ToInt16(attribute);
                                        valueOption.valor = value;
                                        db.mage_atributo_opcion.Add(valueOption);
                                        if (attrVal.value_int != valueOption.id_opcion)
                                        {
                                            modified = true;
                                            attrVal.value_int = valueOption.id_opcion;
                                        }
                                        //attrVal.value_int = valueOption.id_opcion;
                                    }
                                }
                                else
                                {
                                    if (attrVal.value_int != Convert.ToInt32(value))
                                    {
                                        modified = true;
                                        attrVal.value_int = Convert.ToInt32(value);
                                    }
                                }
                                break;
                            case "decimal":
                                if (attrVal.value_decimal != Convert.ToDecimal(value))
                                {
                                    modified = true;
                                    attrVal.value_decimal = Convert.ToDecimal(value);
                                }
                                break;
                            case "varchar":
                                if (attrVal.value_varchar != value)
                                {
                                    modified = true;
                                    attrVal.value_varchar = value;
                                }
                                break;
                            case "text":
                                if (attrVal.value_text != value)
                                {
                                    modified = true;
                                    attrVal.value_text = value;
                                }
                                break;
                        }
                    }
                    if (toUpdate && modified)
                    {
                        db.Entry(attrVal).State = EntityState.Modified;
                    }
                    else if(!toUpdate)
                    {
                        article.mage_articulo_atributo_valor.Add(attrVal);
                        db.mage_articulo_atributo_valor.Add(attrVal);
                    }
                    // article.mage_articulo_atributo_valor.Add(attrVal);
                }
                else
                {
                    ((List<Message>)ViewData[Message.WARNING_TYPE])
                        .Add(
                            new Message(
                                String.Empty,
                                String.Format(Message.WARNING_ATTRIBUTE_FOR_PRODUCT_NOT_EXIST, attribute, article.sku)
                                )
                            );
                }
            }   
        }


        private void UpdateFamilyToProductWithOutSavingChange(List<mage_articulo> articles)
        {
            foreach (mage_articulo article in articles)
            {
                UpdateFamilyToProductWithOutSavingChange(article);
            }
        }
        private Task UpdateFamilyToProductWithOutSavingChangeAsync(mage_articulo article)
        {
            return Task.Run(() => UpdateFamilyToProductWithOutSavingChange(article));
        }
        private void UpdateFamilyToProductWithOutSavingChange(mage_articulo article)
        {
            int? familiaAlfa = null;
            var catPrincipleProduct = article.mage_categoria.FirstOrDefault(ca => ca.nivel == 2);
            int idCategoryHerramientasManual = 5628;
            int idCategoryHerramientasElectrcicas = 5629;
            mage_familia familia = null;

            string tmpFamiliaAlfa = db.g733_articulos.FirstOrDefault(p => p.xarticulo_id == article.sku).g733_familiacatalogo;
            if (tmpFamiliaAlfa != null)
            {
                familiaAlfa = Convert.ToInt32(tmpFamiliaAlfa);

                if (familiaAlfa != article.mage_familia.id_familia_alfa)
                {
                    if (familiaAlfa != 1)
                    {
                        familia = db.mage_familia.FirstOrDefault(f => f.id_familia_alfa == familiaAlfa);
                    }
                    else if (
                                familiaAlfa == 1 &&
                                (
                                    (catPrincipleProduct != null) && 
                                    (catPrincipleProduct.entity_id == idCategoryHerramientasManual || catPrincipleProduct.entity_id == idCategoryHerramientasElectrcicas)
                                )
                            )
                    {
                        familia = db.mage_familia.FirstOrDefault(f => f.nombre == db.mage_categoria.FirstOrDefault(c => c.entity_id == catPrincipleProduct.entity_id).nombre);
                    }
                    else
                    {
                        AddMessagesForView(Message.ERROR_TYPE, new List<Message>() {
                            new Message(
                                        article.sku,
                                        GetMessage(Message.WARNING_FAMILY_NOT_ADDED, new List<string> { article.sku, article.sku }),
                                        Message.GetBtnAction(article.sku, "/Producto/...", "Actualizar Familia")
                                        )
                            });
                    }

                    if (familia != null)
                    {
                        article.id_familia = familia.id_familia;
                        article.mage_familia = familia;
                    }
                }
            }
            else
            {
                article.id_familia = 4;
                AddMessagesForView(Message.ERROR_TYPE, new List<Message>() {
                        new Message(
                                    article.sku,
                                    GetMessage(Message.WARNING_FAMILY_NOT_ADDED, new List<string> { article.sku, article.sku }),
                                    Message.GetBtnAction(article.sku, "/Producto/...", "Actualizar Familia")
                                    )
                    });
            }
        }


        private Task AddFamilyToProductWithoutSavingChangeAsync(List<mage_articulo> articles)
        {
            return Task.Run(() => AddFamilyToProductWithoutSavingChange(articles));
        }
        private void AddFamilyToProductWithoutSavingChange(mage_articulo article)
        {
            AddFamilyToProductWithoutSavingChange(new List<mage_articulo> { article });
        }
        private void AddFamilyToProductWithoutSavingChange(List<mage_articulo> articles)
        {
            int? familiaAlfa = null;
            int idCategoryHerramientasManual = 5628;
            int idCategoryHerramientasElectrcicas = 5629;
            mage_familia familia = null;

            foreach (mage_articulo article in articles)
            {
                var catPrincipleProduct = article.mage_categoria.FirstOrDefault(ca => ca.nivel == 2);
                string tmpFamiliaAlfa = db.g733_articulos.FirstOrDefault(p => p.xarticulo_id == article.sku).g733_familiacatalogo;

                if (tmpFamiliaAlfa != null)
                {
                    familiaAlfa = Convert.ToInt32(tmpFamiliaAlfa);

                    if (familiaAlfa != 1)
                    {
                        familia = db.mage_familia.FirstOrDefault(f => f.id_familia_alfa == familiaAlfa);
                    }
                    // obtener familia apartir de la categoría 1 pasada como parámetro, si es 5628 poner la familia 'x' y si es 5629 poner la familia 'z'
                    else if (
                                familiaAlfa == 1 &&
                                (
                                    (catPrincipleProduct != null) &&
                                    (catPrincipleProduct.entity_id == idCategoryHerramientasManual || catPrincipleProduct.entity_id == idCategoryHerramientasElectrcicas)
                                )
                            )
                    {
                        familia = db.mage_familia.FirstOrDefault(f => f.nombre == db.mage_categoria.FirstOrDefault(c => c.entity_id == catPrincipleProduct.entity_id).nombre);
                    }

                    if (familia != null)
                    {
                        article.id_familia = familia.id_familia;
                        article.mage_familia = familia;
                    }
                    else
                    {
                        article.id_familia = 4;
                        article.mage_familia = db.mage_familia.FirstOrDefault(f => f.id_familia == 4);
                    }
                }
                else
                {
                    article.id_familia = 4;
                    article.mage_familia = db.mage_familia.FirstOrDefault(f => f.id_familia == 4);
                }

            }
        }

        private void AddMessagesForView(string typeMessage, List<Message> messages)
        {
            if (ViewData[typeMessage] != null && ViewData[typeMessage].GetType() == typeof(List<Message>))
            {
                ((List<Message>)ViewData[typeMessage]).AddRange(messages);
            }
            else
            {
                ViewData[typeMessage] = null;
                ViewData[typeMessage] = new List<Message>();
                ((List<Message>)ViewData[typeMessage]).AddRange(messages);
            }
        }

        private Task AddCommonDataEmptyIfNotExistsInG733WithoutSavingChangeAsync(List<mage_articulo> articles)
        {
            return Task.Factory.StartNew(() => AddCommonDataEmptyIfNotExistsInG733WithoutSavingChange(articles));
        }
        private void AddCommonDataEmptyIfNotExistsInG733WithoutSavingChange(mage_articulo article)
        {
            AddCommonDataEmptyIfNotExistsInG733WithoutSavingChange(new List<mage_articulo> { article });
        }
        private void AddCommonDataEmptyIfNotExistsInG733WithoutSavingChange(List<mage_articulo> articles)
        {
            mage_articulo_atributo_valor attrValue;

            foreach (mage_articulo article in articles)
            {
                int i = 0;
                do
                {
                    int idAttrG733 = COMMON_ATTRIBUTES[i];
                    attrValue = article.mage_articulo_atributo_valor.FirstOrDefault(a => a.id_atributo == idAttrG733);
                    if (attrValue == null)
                    {
                        attrValue = new mage_articulo_atributo_valor();
                        attrValue.sku = article.sku;
                        attrValue.id_atributo = Convert.ToInt16(idAttrG733);
                        attrValue.mage_atributo = db.mage_atributo.FirstOrDefault(a => a.id_atributo == idAttrG733);
                        db.mage_articulo_atributo_valor.Add(attrValue);
                        db.SaveChanges();
                    }
                    i++;
                } while (i < COMMON_ATTRIBUTES.Length);
            }
        }

        private Task UpdateCommonDataInG733WithoutSavingChangeAsync(mage_articulo article)
        {
            return Task.Factory.StartNew(() => UpdateCommonDataInG733WithoutSavinChange(article));
        }
        private Task UpdateCommonDataInG733WithoutSavinChangeAsync(List<mage_articulo> articles)
        {
            return Task.Factory.StartNew(() => UpdateCommonDataInG733WithoutSavinChange(articles));
        }
        private void UpdateCommonDataInG733WithoutSavinChange(mage_articulo article)
        {
            UpdateCommonDataInG733WithoutSavinChange(new List<mage_articulo> { article });
        }
        private void UpdateCommonDataInG733WithoutSavinChange(List<mage_articulo> articles)
        {
            foreach (mage_articulo article in articles)
            {
                db.mage_update_common_data_G733(UPDATE_PROMOTIONS_YES, article.sku);

                var attributeValues = from attributeValue in article.mage_articulo_atributo_valor.AsEnumerable()
                                      join commonAttribute in COMMON_ATTRIBUTES
                                      on attributeValue.id_atributo equals commonAttribute
                                      select attributeValue;

                foreach (mage_articulo_atributo_valor attrVal in attributeValues)
                {
                    db.Entry(attrVal).Reload();
                }
            }
        }

        private void SetProductLikeNew(mage_articulo article)
        {
            SetProductLikeNew(new List<mage_articulo> { article });
        }
        private void SetProductLikeNew(List<mage_articulo> articles)
        {
            string valueForSetCategoryNewToAllProducts = "10";
            byte idAttributePromotion = 139;
            foreach (mage_articulo article in articles)
            {
                var attrVal = article.mage_articulo_atributo_valor.FirstOrDefault(a => a.id_atributo == idAttributePromotion);
                if (attrVal != null && attrVal.value_text != null && !attrVal.value_text.Contains(valueForSetCategoryNewToAllProducts))
                {
                    attrVal.value_text += String.Format(",{0}", valueForSetCategoryNewToAllProducts);

                }
                else if (attrVal != null && attrVal.value_text == null)
                {
                    attrVal.value_text = valueForSetCategoryNewToAllProducts;
                }
                db.Entry(attrVal).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public async Task<ActionResult> GetSelectListCategoriesForOnlyParent(int parent)
        {
            ViewBag.CategoriesSelectList = new SelectList(db.mage_categoria.Where(c => c.padre == parent).OrderBy(c => c.nombre), "entity_id", "nombre");
            return PartialView("_ListCatgories", await db.mage_categoria.FirstOrDefaultAsync(c => c.entity_id == parent));
        }

   
        public ActionResult GetProductByIdOrName(string skuOrName = null)
        {
            var articles = db.mage_articulo.Where(a => a.sku.Length == 5).Take(TOTAL_ROW_IN_TABLE_COPY_DATA_PRODUCT);
            if (skuOrName != null)
            {
                ViewBag.ProductsToTableCopyData = db.mage_articulo.Where(p => p.sku.Length == 5 && (p.sku.Contains(skuOrName) || p.mage_articulo_atributo_valor.FirstOrDefault(a => a.id_atributo == 71).value_varchar.Contains(skuOrName))).Take(TOTAL_ROW_IN_TABLE_COPY_DATA_PRODUCT);               
            }
            return PartialView("_TableCopyProductData");
        }

        /// <summary>
        /// Method to return partial view containg the attributes product by family
        /// </summary>
        /// <param name="idFamily"></param>
        /// <returns></returns>
        public async Task<ActionResult> AttributesProduct(int? idFamily)
        {

            if (idFamily == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.Familias = await db.mage_familia.FirstOrDefaultAsync(f => f.id_familia == idFamily);
            if (ViewBag.Familias == null)
            {
                return HttpNotFound();
            }

            return PartialView("_AttributesProduct");
        }

        /// <summary>
        /// Method to insert the value for entity_id attribute to all product without it.
        /// </summary>
        private void UpdateProductEntityIdEqualsToNull()
        {
            List<Parameter> parameters = new List<Parameter>();
            // actualizar entity_id de productos nuevos en jhon.mage_articulos
            var articleWithoutEntity = db.mage_articulo.Where(p => p.entity_id == null);

            foreach (var art in articleWithoutEntity)
            {
                // buscar entity_id del producto de la iteración actual
                int entityId = 0;
                try
                {
                    parameters.Clear();
                    parameters.Add(new Parameter("@sku", MySqlDbType.VarChar, 65532, 0, 0, art.sku));
                    entityId = Convert.ToInt32(dbMysql.ExecuteSimpleQuery(QUERY_GET_ENTITY_FOR_PRODUCT, Parameter.CreateParametersMysql(parameters)));
                }
                catch (Exception ex)
                {
                    // Save Warning to view
                    ViewData["messageWarning"] = null;
                    ViewData["messageWarning"] = new List<Message>();
                    ((List<Message>)ViewData["messageWarning"])
                        .Add(
                            new Message(
                                String.Empty,
                                Message.ERROR_UPDATE_ENTITY_ID_PRODUCTS_IN_ALFA,
                                Message.GetBtnAction(String.Empty, "/Producto/...", "Actualizar IDs Entidades")
                                )
                            );
                }
                finally
                {
                    dbMysql.Dispose(); parameters.Clear();
                }

                if (entityId != 0)
                {
                    // actualizar su entity_id
                    art.entity_id = entityId;
                    // guardar los cambios
                    db.Entry(art).State = EntityState.Modified;
                }
            }
            db.SaveChanges();
        }

        private Task UpdateProductEntityIdEqualsToNullAsync(List<mage_articulo> articles)
        {
            return Task.Run(() => UpdateProductEntityIdEqualsToNull(articles));
        }
        private void UpdateProductEntityIdEqualsToNull(mage_articulo articles)
        {
            UpdateProductEntityIdEqualsToNull(new List<mage_articulo> { articles });
        }
        private void UpdateProductEntityIdEqualsToNull(List<mage_articulo> articles)
        {
            var dbMysql = new DbMySQLServer();
            List<Parameter> parameters = new List<Parameter>();

            foreach (mage_articulo article in articles)
            {
                int entityId = 0;
                try
                {
                    parameters.Clear();
                    parameters.Add(new Parameter("@sku", MySqlDbType.VarChar, 65532, 0, 0, article.sku));
                    entityId = Convert.ToInt32(dbMysql.ExecuteSimpleQuery(QUERY_GET_ENTITY_FOR_PRODUCT, Parameter.CreateParametersMysql(parameters)));
                }
                catch (Exception ex)
                {
                    AddMessagesForView(Message.WARNING_TYPE, 
                        new List<Message>() {
                                    new Message(
                                                article.sku,
                                                Message.ERROR_UPDATE_ENTITY_ID_PRODUCTS_IN_ALFA,
                                                Message.GetBtnAction(String.Empty, "/Producto/...", "Reintentar")
                                                )
                        });
                }
                finally
                {
                    dbMysql.Dispose(); 
                }

                if (entityId != 0)
                {
                    article.entity_id = entityId;
                    db.Entry(article).State = EntityState.Modified;
                }
            }
            dbMysql = null;
            db.SaveChanges();
        }

        private void SetMessageWarningIfFamilySentNotEqualFamilyAlfaForProducts(mage_articulo article, int idFamilySent)
        {
            SetMessageWarningIfFamilySentNotEqualFamilyAlfaForProducts(new List<mage_articulo> { article }, idFamilySent);
        }
        private void SetMessageWarningIfFamilySentNotEqualFamilyAlfaForProducts(List<mage_articulo> articles, int idFamilySent)
        {
            foreach (mage_articulo article in articles)
            {
                if (idFamilySent != article.id_familia)
                {
                    var familySent = db.mage_familia.FirstOrDefault(f => f.id_familia == idFamilySent);
                    var familySentName = "¡none!";
                    if (familySent != null)
                    {
                        familySentName = familySent.nombre;
                    }
                    AddMessagesForView(Message.WARNING_TYPE, new List<Message>() {
                                    new Message(
                                                article.sku,
                                                GetMessage(Message.WARNING_FAMILY_INCORRECT_FOR_PRODUCT, new List<string> { familySentName, article.mage_familia.nombre })
                                                )
                                });
                }
            }
        }

        private void SetViewBagForCreateView()
        {
            ViewBag.FamiliasSelectList = new SelectList(db.mage_familia.Where(f => f.id_familia != 4).OrderBy(f => f.nombre), "id_familia", "nombre");
            ViewBag.CategoriesFirstSelectList = new SelectList(db.mage_categoria.Where(c => c.nivel == 2).OrderBy(c => c.nombre), "entity_id", "nombre");
            ViewBag.Familias = db.mage_familia.Where(f => f.id_familia != 4).OrderBy(f => f.nombre).First();
            ViewBag.ProductsToTableCopyData = db.mage_articulo.Where(a => a.sku.Length == 5).Take(TOTAL_ROW_IN_TABLE_COPY_DATA_PRODUCT).ToList();
        }
        
        /// <summary>
        /// Method to create product or update through excel
        /// </summary>
        /// <param name="excelNewProduct"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateThroughExcel(HttpPostedFileBase excelNewProduct)
        {
            var table = LoadExcelInDataTable(excelNewProduct);
            if (table != null)
                await CreateOrUpdateProductThroughDataTable(table);

            SetViewBagForCreateView();
            return View("Create");
        }


        [ValidateInput(false)]
        [HttpPost]
        public async Task<ActionResult> Create(string dataNewProduct)
        {
            ViewData[Message.WARNING_TYPE] = null;
            ViewData[Message.ERROR_TYPE] = null;
            ViewData[Message.SUCCESS_TYPE] = null;
            var attributesNotNulls = dataNewProduct.Split('&').AsEnumerable();
            var skuFound = attributesNotNulls.Where(a => a.Contains("sku")).ToArray();
            var idFamilia = attributesNotNulls.Where(a => a.Contains("id_familia")).ToArray()[0].Split('=')[1];
            var categories = attributesNotNulls.Where(a => a.Contains("cat")).ToArray();
            attributesNotNulls = attributesNotNulls.Where(a => !(a.Contains("id_familia") || a.Contains("sku") || a.Contains("cat")));

            if (skuFound.Length == 1)
            {
                string sku = skuFound[0].Split('=')[1];
                if (sku.Length > 0) {
                    
                    var article = db.mage_articulo.FirstOrDefault(a => a.sku == sku);
                    if (article == null)
                    {
                        article = GetProductOrEmptyProductIfNotExitsSavingChange(sku);
                        if (article != null)
                        {
                            AddCategoriesToProductWithoutSavingChange(article, categories);
                            AddFamilyToProductWithoutSavingChange(article);
                            AddCommonDataEmptyIfNotExistsInG733WithoutSavingChange(article);
                            AddAttributesToProductWithoutSavingChange(article, attributesNotNulls);

                            db.SaveChanges();
                            await UpdateCommonDataInG733WithoutSavingChangeAsync(article);
                            SetProductLikeNew(article);

                            bool someProductWasUploadCorrectly = await UploadDataProductsInTemporaryTableOnMagentoAsync(article, String.Format("Introducción manual artículo: {0}", article.sku));
                            if (someProductWasUploadCorrectly)
                            {
                                LocateAllAttributesInSpecicTablesForNewProducts(String.Format("Introducción manual artículo: {0}", article.sku), article);
                                UpdateProductEntityIdEqualsToNull(article);
                            }
                        
                            if (idFamilia != null)
                            {
                                SetMessageWarningIfFamilySentNotEqualFamilyAlfaForProducts(article, Convert.ToInt16(idFamilia));
                            }
                        }
                        else
                        {
                            ViewData[Message.ERROR_TYPE] = String.Format(Message.ERROR_PRODUCT_NOT_EXIST_IN_G733, sku);
                        }
                    }
                    else
                    {
                        ViewData[Message.ERROR_TYPE] = String.Format(Message.ERROR_PRODUCT_ALREADY_EXIST_IN_G733, sku);
                    }
                }
                else
                {
                    ViewData[Message.ERROR_TYPE] = String.Format(Message.ERROR_PRODUCT_CODE_CANT_BE_EMPTY);
                }
            }
            else
            {
                ViewData[Message.ERROR_TYPE] = String.Format(Message.ERROR_SEND_ONLY_CODE_PRODUCT);
            }

            SetViewBagForCreateView();
            return PartialView("_Messages");

        }

        // GET: Producto/Create
        public ActionResult Create()
        {
            SetViewBagForCreateView();
            return PartialView();
        }


        /// <summary>
        /// Method to insert common data in G733_DATA.jhon.mage_articulo_atributo_valor and table ad_new_update_product on Magento.
        /// </summary>
        /// <param name="attributesValue"></param>
        /// <param name="sku"></param>
        /// <param name="dateParam"></param>
        private void InsertCommonDataInG733AndMagento(ICollection<mage_articulo_atributo_valor> attributesValue, string sku, string fileName, string dateParam=null)
        {
            string date = dateParam != null ? dateParam : DateTime.Today.ToString("yyyyMMdd");
            List<Parameter> parameters = new List<Parameter>();
            const int MANUFACTURE = 81;
            const int PRICE = 75;
            const int UNIDAD_ENVACE = 823;
            const int STATUS = 96;
            const int IS_MULTIPLE = 824;
            const int EAN = 144;
            const int PROMOTIONS = 139;
            const int ORDEN = 836;
            const int VISIBILITY = 102;

            var attrG733 = new int[] { MANUFACTURE, PRICE, UNIDAD_ENVACE, STATUS, IS_MULTIPLE, EAN, PROMOTIONS, ORDEN, VISIBILITY };

            int i = 0;
            do
            {
                int idAttrG733 = attrG733[i];
                var attrValue = attributesValue.FirstOrDefault(a => a.id_atributo == idAttrG733);
                if (attrValue == null)
                {
                    attrValue = new mage_articulo_atributo_valor();
                    attrValue.sku = sku;
                    attrValue.id_atributo = Convert.ToInt16(idAttrG733);
                    attrValue.mage_atributo = db.mage_atributo.FirstOrDefault(a => a.id_atributo == idAttrG733);
                    db.mage_articulo_atributo_valor.Add(attrValue);
                    db.SaveChanges();
                    db.mage_update_common_data_G733((idAttrG733 == PROMOTIONS ? 1 : 0), sku);
                    db.Entry(attrValue).Reload();
                }

                string value = "";
                switch (idAttrG733)
                {
                    case 75:
                        value = attrValue.value_decimal != null ? attrValue.value_decimal.ToString().Replace(",",".") : value;
                        break;
                    case 139:
                        value = attrValue.value_text != null ? attrValue.value_text.ToString() : value;
                        break;
                    case 81:
                        var option = db.mage_atributo_opcion.FirstOrDefault(o => o.id_opcion == attrValue.value_int && o.id_atributo == 81);
                        if (option != null)
                            value = option.valor;
                        break;
                    case 96:
                    case 824:
                    case 836:
                    case 102:
                        value = attrValue.value_int != null ? attrValue.value_int.ToString() : value;
                        break;
                    default:
                        value = attrValue.value_varchar ?? value;
                        break;
                }

                parameters.Add(new Parameter("@sku", MySqlDbType.VarChar, 5, 5, 0, sku));
                parameters.Add(new Parameter("@date", MySqlDbType.VarChar, 5, 5, 0, date));
                parameters.Add(new Parameter("@atributo", MySqlDbType.Int32, 5, 5, 0, attrG733[i]));
                parameters.Add(new Parameter("@value", MySqlDbType.VarChar, 65532, 0, 0, value));
                parameters.Add(new Parameter("@fileName", MySqlDbType.VarChar, 65532, 0, 0, fileName));

                try
                {
                    dbMysql.ExecuteSentenceCommand(QUERY_INSERT_DATA_ATTRIBUTE_NEW_OR_UPDATE_MAGENTO, Parameter.CreateParametersMysql(parameters));
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
                finally
                {
                    dbMysql.Dispose(); parameters.Clear();
                }

                i++;
            } while (i < attrG733.Length);
        }

        /// <summary>
        /// Method to Insert Category in G733_DATA.jhon_mage_categoria_articulo
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="level"></param>
        /// <param name="article"></param>
        private void InsertCategoryInG733Data(int entityId, int level, mage_articulo article)
        {
            var category = article.mage_categoria.FirstOrDefault(c => c.entity_id == entityId);
            if (category == null)
            {
                var catWithSameLevel = article.mage_categoria.FirstOrDefault(c => c.nivel == level);
                if (catWithSameLevel != null)
                    article.mage_categoria.Remove(catWithSameLevel);

                category = db.mage_categoria.FirstOrDefault(c => c.entity_id == entityId);
                if (category != null)
                    article.mage_categoria.Add(category);

                if (catWithSameLevel != null || category != null)
                {
                    db.Entry(article).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Method to Insert data attribute in G733_DATA.jhon.mage_articulo_atributo_valor.
        /// </summary>
        /// <param name="attributesValue"></param>
        /// <param name="sku"></param>
        /// <param name="atributo"></param>
        /// <param name="value"></param>
        private void InsertAttributeValueInG733Data(ICollection<mage_articulo_atributo_valor> attributesValue, string sku, int atributo, string value)
        {
            var attrValue = attributesValue.FirstOrDefault(a => a.id_atributo == atributo);

            if (attrValue == null)
            {
                attrValue = new mage_articulo_atributo_valor();
                attrValue.sku = sku;
                attrValue.id_atributo = Convert.ToInt16(atributo);
                attrValue.mage_atributo = db.mage_atributo.FirstOrDefault(a => a.id_atributo == atributo);
                db.mage_articulo_atributo_valor.Add(attrValue);
                db.SaveChanges();
            }

            switch (attrValue.mage_atributo.tipo)
            {
                case "int":
                    if (attrValue.mage_atributo.id_atributo == 92 || attrValue.mage_atributo.filtrable == 1)
                    {
                        var valueOption = db.mage_atributo_opcion.FirstOrDefault(o => o.valor == value && o.id_atributo == attrValue.id_atributo);
                        if (valueOption != null)
                        {
                            attrValue.value_int = valueOption.id_opcion;
                        }
                        else
                        {
                            valueOption = new mage_atributo_opcion();
                            valueOption.id_opcion = db.mage_atributo_opcion.OrderByDescending(o => o.id_opcion).First().id_opcion + 1;
                            valueOption.id_atributo = Convert.ToInt16(atributo);
                            valueOption.valor = value;
                            db.mage_atributo_opcion.Add(valueOption);
                            db.SaveChanges();
                            attrValue.value_int = attrValue.mage_atributo.mage_atributo_opcion.FirstOrDefault(o => o.valor == value).id_opcion;
                        }
                    }
                    else
                    {
                        attrValue.value_int = Convert.ToInt32(value);
                    }
                    break;
                case "decimal":
                    attrValue.value_decimal = Convert.ToDecimal(value);
                    break;
                case "varchar":
                    attrValue.value_varchar = value;
                    break;
                default:
                    attrValue.value_text = value;
                    break;
            }

            db.Entry(attrValue).State = EntityState.Modified;
            db.SaveChanges();
        }

        private string[] GetCategoriesFromExcel(DataRow row) {
            List<string> categories = new List<string>();
            string nameColumnCat1 = "cat1";
            string nameColumnCat2 = "cat2";
            string nameColumnCat3 = "cat3";

            if (row.Table.Columns.Contains(nameColumnCat1) && !String.IsNullOrWhiteSpace(row[nameColumnCat1].ToString()))
            {
                categories.Add(String.Format("{0}={1}",nameColumnCat1,row[nameColumnCat1]));
            }
            if (row.Table.Columns.Contains(nameColumnCat1) && !String.IsNullOrWhiteSpace(row[nameColumnCat2].ToString()))
            {
                categories.Add(String.Format("{0}={1}", nameColumnCat2,row[nameColumnCat2]));
            }
            if (row.Table.Columns.Contains(nameColumnCat1) && !String.IsNullOrWhiteSpace(row[nameColumnCat3].ToString()))
            {
                categories.Add(String.Format("{0}={1}", nameColumnCat3,row[nameColumnCat3]));
            }
            return categories.Count > 0 ? categories.ToArray() : null;
        }

        private IEnumerable<string> GetAttributeFromExcel(string sku, DataRow row/*, DataColumnCollection columns*/)
        {
            List<string> attributes = new List<string>();
            
            //foreach (DataColumn col in columns)
            foreach (DataColumn col in row.Table.Columns)
            {
                if (col.ColumnName.Contains("SKU") || IsItSomeCategoryColumn(col.ColumnName) || col.ColumnName.Contains("Column")) { continue; }
                else
                {
                    try
                    {
                        var attribute = db.mage_atributo.FirstOrDefault(a => a.nombre == col.ColumnName).id_atributo;
                        var value = Convert.ToString(row[col.ColumnName]);
                        attributes.Add(String.Format("{0}={1}", attribute, value));
                    }
                    catch (Exception ex)
                    {
                        ((List<Message>)ViewData[Message.ERROR_TYPE])
                        .Add(
                            new Message(
                                sku,
                                String.Format(Message.ERROR_COLUMN_NOT_MATCH_WITH_ANY_ATTRIBUTE, col.ColumnName, col.ColumnName)
                                )
                            );
                        attributes = null;
                        break;
                    }
                }
            }
            return attributes;
        }

        /// <summary>
        /// Method to create or update product in MAgento and Alfa Dyser
        /// </summary>
        /// <param name="excel"></param>
        public async Task<Dictionary<string,List<Message>>> CreateOrUpdateProductThroughDataTable(DataTable excel)
        {
            ViewData[Message.WARNING_TYPE] = new List<Message>();
            ViewData[Message.ERROR_TYPE] = new List<Message>();
            ViewData[Message.SUCCESS_TYPE] = new List<Message>();
            mage_articulo article = null;
            string[] categories = null;
            IEnumerable<string> attributes = null;
            List<mage_articulo> articlesToUpload = new List<mage_articulo>();
            string fileName = excel.TableName;
            string sku = String.Empty;

            foreach (DataRow row in excel.Rows)
            {
                if (String.IsNullOrWhiteSpace(row["sku"].ToString())) {
                    break;
                }

                sku = row["sku"].ToString();

                article = GetProductOrEmptyProductIfNotExitsSavingChange(sku);
                if (article != null)
                {
                    categories = GetCategoriesFromExcel(row);
                    attributes = GetAttributeFromExcel(sku, row/*, excel.Columns*/);

                    if (attributes != null)
                    {
                        try
                        {
                            AddCategoriesToProductWithoutSavingChange(article, categories);
                            AddFamilyToProductWithoutSavingChange(article);
                            AddCommonDataEmptyIfNotExistsInG733WithoutSavingChange(article);
                            AddAttributesToProductWithoutSavingChange(article, attributes, true);
                            // SET ETIQUETAS
                            AddTagsSearchToProducts(article); // to method create individually
                            articlesToUpload.Add(article);

                            bool saveFailed;
                            do
                            {
                                saveFailed = false;

                                try
                                {
                                    db.SaveChanges();
                                }
                                catch (DbUpdateConcurrencyException ex)
                                {
                                    saveFailed = true;

                                    // Update the values of the entity that failed to save from the store
                                    ex.Entries.Single().Reload();
                                }

                            } while (saveFailed);
                        
                        }
                        catch (Exception ex)
                        {
                            ((List<Message>)ViewData[Message.ERROR_TYPE])
                                .Add(
                                    new Message(
                                        sku,
                                        String.Format(Message.ERROR_PRODUCT_NOT_ADDED_BECAUSE_THERE_IS_SOME_DATA_INCORRECTE, sku)
                                        )
                                    );
                        }
                    }
                }
                else
                {
                    ((List<Message>)ViewData[Message.ERROR_TYPE])
                        .Add(
                            new Message(
                                sku,
                                String.Format(Message.ERROR_PRODUCT_NOT_EXIST_IN_G733, sku)
                                )
                            );
                    
                }

            }

            if (articlesToUpload.Count() > 0)
            {
                UpdateCommonDataInG733WithoutSavinChange(articlesToUpload);
                SetProductLikeNew(articlesToUpload);
                bool someProductWasUploadCorrectly = await UploadDataProductsInTemporaryTableOnMagentoAsync(articlesToUpload, fileName);
                if (someProductWasUploadCorrectly)
                {
                    LocateAllAttributesInSpecicTablesForNewProducts(fileName, articlesToUpload);
                    UpdateProductEntityIdEqualsToNull(articlesToUpload);
                }
            }


            return new Dictionary<string, List<Message>> { { Message.WARNING_TYPE, (List<Message>)ViewData[Message.WARNING_TYPE] },
                                                            { Message.ERROR_TYPE, (List<Message>)ViewData[Message.ERROR_TYPE] },
                                                            { Message.SUCCESS_TYPE, (List<Message>)ViewData[Message.SUCCESS_TYPE] } };


        }

        private void AddTagsSearchToProducts(mage_articulo article)
        {
            AddTagsSearchToProducts(new List<mage_articulo> { article });
        }

        private void AddTagsSearchToProducts(List<mage_articulo> articles)
        {
            foreach (mage_articulo article in articles)
            {
                var attrValTag = article.mage_articulo_atributo_valor.FirstOrDefault(a => a.id_atributo == 201);
                if (attrValTag != null)
                {
                    string ean = article.mage_articulo_atributo_valor.FirstOrDefault(a => a.id_atributo == 144) != null 
                        ? article.mage_articulo_atributo_valor.FirstOrDefault(a => a.id_atributo == 144).value_varchar : null;

                    string name = article.mage_articulo_atributo_valor.FirstOrDefault(a => a.id_atributo == 71) != null
                        ? article.mage_articulo_atributo_valor.FirstOrDefault(a => a.id_atributo == 71).value_varchar : null;

                    var manufacture = article.mage_articulo_atributo_valor.FirstOrDefault(a => a.id_atributo == 81) != null
                        ? article.mage_articulo_atributo_valor.FirstOrDefault(a => a.id_atributo == 81) : null;
                    string valueManufacture = null;
                    if (manufacture != null)
                    {
                        valueManufacture = manufacture.mage_atributo.mage_atributo_opcion.FirstOrDefault(o => o.id_opcion == manufacture.value_int) != null 
                            ? manufacture.mage_atributo.mage_atributo_opcion.FirstOrDefault(o => o.id_opcion == manufacture.value_int).valor : null;
                    }

                    string family = article.mage_familia != null ? article.mage_familia.nombre : null;

                    string valueToAdd = String.Empty;
                    valueToAdd += article.sku;
                    valueToAdd += ean != null ? String.Format(",{0}",ean) : String.Empty;
                    valueToAdd += name != null ? String.Format(",{0}",name.Replace(" ", ",")) : String.Empty;
                    valueToAdd += valueManufacture != null ? String.Format(",{0}",valueManufacture) : String.Empty;
                    valueToAdd += family != null ? String.Format(",{0}",family) : String.Empty;


                    attrValTag.value_varchar = attrValTag.value_varchar != null ? String.Format("{0},{1}",attrValTag.value_varchar,valueToAdd) : valueToAdd;

                }
            }
        }

        // GET: Producto/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            mage_articulo mage_articulo = await db.mage_articulo.FindAsync(id);
            if (mage_articulo == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_familia = new SelectList(db.mage_familia, "id_familia", "nombre", mage_articulo.id_familia);

            var cat1 = mage_articulo.mage_categoria.FirstOrDefault(c => c.nivel == 2);
            var cat2 = mage_articulo.mage_categoria.FirstOrDefault(c => c.nivel == 3);
            var cat3 = mage_articulo.mage_categoria.FirstOrDefault(c => c.nivel == 4);
            var cat1Def = db.mage_categoria.FirstOrDefault(c => c.nivel == 2);
            var cat2Def = db.mage_categoria.FirstOrDefault(c => c.nivel == 3 && c.padre == cat1Def.entity_id);
            var cat3Def = db.mage_categoria.FirstOrDefault(c => c.nivel == 4 && c.padre == cat2Def.entity_id);

            if (cat1 != null)
            {
                ViewBag.CategoriesFirstSelectList = new SelectList(db.mage_categoria.Where(c => c.nivel == 2).OrderBy(c => c.nombre), "entity_id", "nombre", cat1.entity_id);
                if (cat2 != null)
                {
                    ViewBag.CategoriesSecondSelectList = new SelectList(db.mage_categoria.Where(c => c.nivel == 3 && c.padre == cat2.padre).OrderBy(c => c.nombre), "entity_id", "nombre", cat2.entity_id);
                    if (cat3 != null)
                    {
                        ViewBag.CategoriesThirdSelectList = new SelectList(db.mage_categoria.Where(c => c.nivel == 4 && c.padre == cat3.padre).OrderBy(c => c.nombre), "entity_id", "nombre", cat3.entity_id);
                    }
                    else
                    {
                        ViewBag.CategoriesThirdSelectList = new SelectList(db.mage_categoria.Where(c => c.nivel == 4 && c.padre == cat2Def.entity_id).OrderBy(c => c.nombre), "entity_id", "nombre", cat3Def.entity_id);
                    }
                }
                else
                {
                    ViewBag.CategoriesSecondSelectList = new SelectList(db.mage_categoria.Where(c => c.nivel == 3 && c.padre == cat1Def.entity_id).OrderBy(c => c.nombre), "entity_id", "nombre", cat2Def.entity_id);
                    ViewBag.CategoriesThirdSelectList = new SelectList(db.mage_categoria.Where(c => c.nivel == 4 && c.padre == cat2Def.entity_id).OrderBy(c => c.nombre), "entity_id", "nombre", cat3Def.entity_id);
                } 
            }
            else
            {
                ViewBag.CategoriesFirstSelectList = new SelectList(db.mage_categoria.Where(c => c.nivel == 2).OrderBy(c => c.nombre), "entity_id", "nombre", cat1Def.entity_id);
                ViewBag.CategoriesSecondSelectList = new SelectList(db.mage_categoria.Where(c => c.nivel == 3 && c.padre == cat1Def.entity_id).OrderBy(c => c.nombre), "entity_id", "nombre", cat2Def.entity_id);
                ViewBag.CategoriesThirdSelectList = new SelectList(db.mage_categoria.Where(c => c.nivel == 4 && c.padre == cat2Def.entity_id).OrderBy(c => c.nombre), "entity_id", "nombre", cat3Def.entity_id);
            }



            ViewBag.ProductsToTableCopyData = db.mage_articulo.Where(a => a.sku.Length == 5).Take(TOTAL_ROW_IN_TABLE_COPY_DATA_PRODUCT).ToList();

            return PartialView(mage_articulo);
        }

        [ValidateInput(false)]
        [HttpPost]
        public async Task<ActionResult> Update(string dataNewProduct)
        {
            ViewData[Message.WARNING_TYPE] = null;
            ViewData[Message.ERROR_TYPE] = null;
            ViewData[Message.SUCCESS_TYPE] = null;
            var attributesNotNulls = dataNewProduct.Split('&').AsEnumerable();
            var skuFound = attributesNotNulls.Where(a => a.Contains("sku")).ToArray();
            var idFamilia = attributesNotNulls.Where(a => a.Contains("id_familia")).ToArray()[0].Split('=')[1];
            var categories = attributesNotNulls.Where(a => a.Contains("cat")).ToArray();
            attributesNotNulls = attributesNotNulls.Where(a => !(a.Contains("id_familia") || a.Contains("sku") || a.Contains("cat")));

            if (skuFound.Length == 1)
            {
                string sku = skuFound[0].Split('=')[1];
                if (sku.Length > 0)
                {
                    var article = GetProductOrEmptyProductIfNotExitsSavingChange(sku);
                    if (article != null)
                    {
                        UpdateCategoriesToProductWithoutSavingChange(article, categories);
                        UpdateFamilyToProductWithOutSavingChange(article); // check out what happen with the family herramientas manual y herramientas electricas
                        AddCommonDataEmptyIfNotExistsInG733WithoutSavingChange(article);
                        UpdateAttributesToProductWithoutSavingChange(article, attributesNotNulls); // update the null attributes as well

                        db.SaveChanges();
                        await UpdateCommonDataInG733WithoutSavingChangeAsync(article);

                        bool someProductWasUploadCorrectly = await UploadDataProductsInTemporaryTableOnMagentoAsync(article, String.Format("Introducción manual artículo: {0}", article.sku));
                        if (someProductWasUploadCorrectly)
                        {
                            LocateAllAttributesInSpecicTablesForNewProducts(String.Format("Introducción manual artículo: {0}", article.sku), article);
                        }

                        if (idFamilia != null)
                        {
                            SetMessageWarningIfFamilySentNotEqualFamilyAlfaForProducts(article, Convert.ToInt16(idFamilia));
                        }

                    }
                    else
                    {
                        ViewData[Message.ERROR_TYPE] = String.Format(Message.ERROR_PRODUCT_NOT_EXIST_IN_G733, sku);
                    }
                }
                else
                {
                    ViewData[Message.ERROR_TYPE] = String.Format(Message.ERROR_PRODUCT_CODE_CANT_BE_EMPTY);
                }
            }
            else
            {
                ViewData[Message.ERROR_TYPE] = String.Format(Message.ERROR_SEND_ONLY_CODE_PRODUCT);
            }

            return PartialView("_Messages");
        }

        [HttpPost]
        public JsonResult CheckCodeAndImageProductToUpdate(string codesProduct, string namesImage) {
            char[] separators = { '\n', ',', ' ' };
            string[] listSku = codesProduct.Split(separators).AsEnumerable().Where(c => !String.IsNullOrEmpty(c)).ToArray(), listImage = namesImage.Split(separators);
            string productoNotUpdated = String.Empty;
            int totalProductNotUpdated = 0;

            foreach (string sku in listSku) {
                if (String.IsNullOrEmpty(sku)) continue;

                var product = db.mage_articulo.FirstOrDefault(p => p.sku == sku);
                if (product == null)
                {
                    productoNotUpdated += String.Format(",{0}",sku);
                    totalProductNotUpdated++;
                    continue;
                }
                var attributeEan = product.mage_articulo_atributo_valor.First(a => a.id_atributo == 144);
                if (attributeEan == null)
                {
                    productoNotUpdated += String.Format(",{0}", sku);
                    totalProductNotUpdated++;
                    continue;
                }
                var ean = attributeEan.value_varchar;
                if (String.IsNullOrEmpty(ean) || String.IsNullOrWhiteSpace(ean)) {
                    productoNotUpdated += String.Format(",{0}", sku);
                    totalProductNotUpdated++;
                    continue;
                }

                bool hasImageAssigned = false;
                foreach (string imageName in listImage) {
                    if (imageName.Contains(ean)) { hasImageAssigned = true; break; }
                }
                if (!hasImageAssigned) { productoNotUpdated += String.Format(",{0}", sku); totalProductNotUpdated++; }
            }

            return Json(new { Code = totalProductNotUpdated == 0 ? 1 : 0,
                TotalProductNotUpdated = totalProductNotUpdated == listSku.Length ? "All" : totalProductNotUpdated.ToString(),
                CodeProducts = totalProductNotUpdated == 0 ? productoNotUpdated : productoNotUpdated.Substring(1)
            });
        }

        private void UpdateSmallImageAndThumbnailAndImageOnDbMysql(string[] listCodeProduct, string[] listImages)
        {
            string sql = "insert into catalog_product_entity_varchar values("+
            "(select * from (select value_id from catalog_product_entity_varchar where attribute_id = {1} and entity_id = (select entity_id from catalog_product_entity where sku = '{0}')) kk), "+
            "4, {1}, 0, (select entity_id from catalog_product_entity where sku = '{0}'), '/8/4/{2}') ON DUPLICATE KEY UPDATE value = '/8/4/{2}'; ";
            int[] attributes = { 85, 86, 87 };

            DbMySQLServer dbMysql = new DbMySQLServer();

            foreach (string sku in listCodeProduct) {
                string ean = db.mage_articulo.FirstOrDefault(p => p.sku == sku).mage_articulo_atributo_valor.First(a => a.id_atributo == 144).value_varchar;
                foreach (string imageName in listImages) {
                    if (imageName.Contains(ean))
                    {
                        foreach (int attributeId in attributes)
                        {
                            string sqlToExecute = String.Format(sql, sku, attributeId, imageName);
                            try
                            {
                                dbMysql.ExecuteSentenceCommand(sqlToExecute, null);
                            }
                            catch (Exception ex)
                            {
                                throw new Exception(ex.Message);
                            }
                            finally
                            {
                                dbMysql.Dispose();
                            }
                        }
                        break;
                    }
                }
            }

            dbMysql = null;
            
        }

        private string[] CheckProductBeforeCallWebServicesUpdateimage(string[] codesProduct, string[] namesImage)
        {
            List<string> result = new List<string>();

            foreach (string sku in codesProduct)
            {
                if (String.IsNullOrEmpty(sku)) continue;

                var product = db.mage_articulo.FirstOrDefault(p => p.sku == sku);
                if (product != null)
                {
                    var attributeEan = product.mage_articulo_atributo_valor.First(a => a.id_atributo == 144);
                    if(attributeEan != null)
                    {
                        var ean = attributeEan.value_varchar;
                        if (!(String.IsNullOrEmpty(ean) || String.IsNullOrWhiteSpace(ean)))
                        {
                            foreach (string imageName in namesImage)
                            {
                                if (imageName.Contains(ean))
                                {
                                    result.Add(sku);
                                    break;
                                }
                            }
                        }
                    }
                    
                }
                
            }
            return result.ToArray();
        }


        [HttpPost]
        public JsonResult UpdateImages(string listCodeProduct, string namesImages)
        {
            char[] separators = { '\n', ',', ' ' };
            string[] listSku = listCodeProduct.Split(separators).AsEnumerable().Where(c => !String.IsNullOrEmpty(c)).ToArray(),
                listImages = namesImages.Split(separators);

            string[] finallyListSku = CheckProductBeforeCallWebServicesUpdateimage(listSku, listImages);

            MagentoServices.PortTypeClient clientMagentoWsdl = new PortTypeClient();
            var sesionMagentoWsdl = clientMagentoWsdl.login("alfadyser", "4lf4dys3r");
            
            string resultProductNotUpdated = String.Empty;
            foreach (string sku in finallyListSku)
            {
                var result = clientMagentoWsdl.call(
                sesionMagentoWsdl,
                "productos.refreshimages",
                sku
                );
                if (result.ToString().Contains("ERROR:"))
                    resultProductNotUpdated += String.Format("{0}, ",sku);
            }
            

            clientMagentoWsdl.endSession(sesionMagentoWsdl);
            sesionMagentoWsdl = string.Empty;
            clientMagentoWsdl = null;

            try
            {
                UpdateSmallImageAndThumbnailAndImageOnDbMysql(finallyListSku, listImages);
            } catch (Exception ex)
            {
                return Json(String.Format("<br>No se ha podido modificar la imagen pequeña de el/los productos, a continuación puede leer el error del sistema gestor de base de datos: <b>{0}</b>",ex.Message));
            }

            return Json(resultProductNotUpdated != String.Empty ? String.Format("<br>- No se ha podido actualizar las imagenes de los siguientes productos: <b>{0}</b>. Ha ocurrido un error en la llamada del WebServices.", resultProductNotUpdated.Substring(0, resultProductNotUpdated.Length-2)) : String.Empty);
        }

        public List<string> GetAllImagesOnFtpServer(string ean)
        {
            string user = "ftp_alfadyser", pass = "fzR9jW94AhNNkyb", server = "ftp://178.255.225.113",
                folderImported = "media/import/",
                folderSaved = "media/catalog/product/8/4/";

            List<string> imageNames = new List<string>();

            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(String.Format("{0}/{1}", server, String.IsNullOrEmpty(ean) ? folderImported : folderSaved)));
                request.Method = WebRequestMethods.Ftp.ListDirectory;
                request.Credentials = new NetworkCredential(user, pass);

                using (WebResponse listResponse = request.GetResponse())
                using (Stream listStream = listResponse.GetResponseStream())
                using (StreamReader listReader = new StreamReader(listStream))
                {
                    while (!listReader.EndOfStream)
                    {
                        string line = listReader.ReadLine();
                        if (line.Contains(".jpg") && line.Length > 17)
                            if (!String.IsNullOrEmpty(ean)) {
                                if (line.Contains(ean)) {
                                    imageNames.Add(line);
                                }
                            }
                            else imageNames.Add(line);
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }

            return imageNames;
        }

        public JsonResult DeteleImageFromFtpServer(string ean)
        {
            string user = "ftp_alfadyser", pass = "fzR9jW94AhNNkyb", server = "ftp://178.255.225.113",
                folderImported = "media/import",
                folderSaved = "media/catalog/product/8/4";

            var listImported = GetAllImagesOnFtpServer(null);
            var listSaved = GetAllImagesOnFtpServer(ean);
            try
            {
                foreach (string image in listImported)
                {
                    string fileName = String.Format("{0}/{1}", folderImported, image);
                    FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(String.Format("{0}/{1}", server, fileName)));
                    request.Method = WebRequestMethods.Ftp.DeleteFile;
                    request.Credentials = new NetworkCredential(user, pass);
                    FtpWebResponse responseFileDelete = (FtpWebResponse)request.GetResponse();
                }
                
                foreach (string image in listSaved)
                {
                    string fileName = String.Format("{0}/{1}", folderSaved, image);
                    FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(String.Format("{0}/{1}", server, fileName)));
                    request.Method = WebRequestMethods.Ftp.DeleteFile;
                    request.Credentials = new NetworkCredential(user, pass);
                    FtpWebResponse responseFileDelete = (FtpWebResponse)request.GetResponse();
                }
            }
            catch (Exception ex)
            {
                return Json(new { Code = 0 });
            }

            return Json(new { Code = 1 });
        }

        [HttpPost]
        public JsonResult UploadImageToFtpServer(string imageName)
        {
            string user = "ftp_alfadyser", pass = "fzR9jW94AhNNkyb", server = "ftp://178.255.225.113", fileName = String.Format("media/import/{0}",imageName);
            Stream ftpstream = null;
            FileStream fs = null;
            
            try {
                
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(String.Format("{0}/{1}", server, fileName)));
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(user, pass);
                ftpstream = request.GetRequestStream();
                fs = System.IO.File.OpenRead(Path.Combine(Server.MapPath("~/Upload/Product/Image"), imageName));

                byte[] buffer = new byte[1024];
                int byteRead = 0;
                double read = 0;
                do
                {
                    byteRead = fs.Read(buffer, 0, 1024);
                    ftpstream.Write(buffer, 0, byteRead);
                    read += (double)byteRead;
                }
                while (byteRead != 0);
            }
            catch (Exception ex)
            {
                return Json(new { Code = 0 });
            }
            finally
            {
                fs.Close();
                ftpstream.Close();
            }

            return Json(new { Code = 1 });
        }

        [HttpGet]
        public ActionResult FormUpdatePhoto()
        {
            return PartialView();
        }

        [HttpPost]
        public  JsonResult UploadImages()
        {
            
            /*if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                if (file != null && file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("~/Upload/Product/Image"), fileName);
                    file.SaveAs(path);
                }
            }
            return Json("Upload file");*/

            try
            {
                string currentEan = "";
                foreach (string file in Request.Files)
                {

                    string ean = file.Substring(0, 13);
                    if (currentEan != ean)
                    {
                        currentEan = ean;
                        DeteleImageFromFtpServer(ean);
                    }


                    var fileContent = Request.Files[file];
                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        // get a stream
                        var stream = fileContent.InputStream;
                        // and optionally write the file to disk
                        var fileName = Path.GetFileName(file);
                        // search directory if not exists create them!
                        if (!System.IO.Directory.Exists( Server.MapPath("~/Upload")))
                        {
                            System.IO.Directory.CreateDirectory(Server.MapPath("~/Upload"));
                            System.IO.Directory.CreateDirectory(Server.MapPath("~/Upload/Product"));
                            System.IO.Directory.CreateDirectory(Server.MapPath("~/Upload/Product/Image"));
                        }
                        else if (!System.IO.Directory.Exists(Server.MapPath("~/Upload/Product")))
                        {
                            System.IO.Directory.CreateDirectory(Server.MapPath("~/Upload/Product"));
                            System.IO.Directory.CreateDirectory(Server.MapPath("~/Upload/Product/Image"));
                        }
                        else if (!System.IO.Directory.Exists(Server.MapPath("~/Upload/Product/Image")))
                        {
                            System.IO.Directory.CreateDirectory(Server.MapPath("~/Upload/Product/Image"));
                        }

                        var path = Path.Combine(Server.MapPath("~/Upload/Product/Image"), fileName);
                        using (var fileStream = new FileStream(path, FileMode.Create))
                        {
                            stream.CopyTo(fileStream);
                        }
                    }
                }
            }
            catch (Exception)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new { Code = 0, Message="Upload failed" });
            }

            return Json(new { Code = 1, Message = "Upload successfully" });
        }

        // POST: Producto/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            mage_articulo mage_articulo = await db.mage_articulo.FindAsync(id);
            db.mage_articulo.Remove(mage_articulo);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }


}
